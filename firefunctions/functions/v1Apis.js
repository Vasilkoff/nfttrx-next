/**
 * @author Maxim Vasilkov maxim@nfttrx.com
 * @project: NFTTRX.com
 * @date: 13th Jan 2022
 */

const safeJsonStringify = require("safe-json-stringify");
const functions = require("firebase-functions");
const firebase = require("firebase-admin");
const { getStorage } = require("firebase-admin/storage");
const cors = require("cors");
const express = require("express");
const axios = require("axios");
const crypto = require("crypto");
const pdf = require("html-pdf");
const mustache = require("mustache");

const opensea = require("api")("@opensea/v1.0#fyyfrc5kydcswq6");

// Let's start express for API
const app = express();

// Automatically allow cross-origin requests
app.use(cors());
app.options("*", cors());

// The var represents firestore db
let db = firebase.firestore();
let storage = firebase.storage();

let stripe_pk =
  "pk_test_51LECBfDaK9yndacRF7yQi4IayXI8DMzdtwBfaI3eJtNjbMJKgY2ihdkvWFVq0uIjVLMe5D2FQyf8ZjYf2rvWgtnd006f5ujw9B";
//sk_test_51LECBfDaK9yndacRLez0GLV3GQrFzUwR8HWxnqW0cVJfPfrEaYwTG5YCw4jTD1mNVa67zLBXKMbLWw1C1n4tl3JU00QfgLbL3U

const stripe = require("stripe")(
  "sk_test_51LECBfDaK9yndacRLez0GLV3GQrFzUwR8HWxnqW0cVJfPfrEaYwTG5YCw4jTD1mNVa67zLBXKMbLWw1C1n4tl3JU00QfgLbL3U"
);

const SUMSUB_BASE_URL = "https://api.sumsub.com";

// Sandbox env
// const SUMSUB_APP_TOKEN =
//     "sbx:dsmQJkH4UXvl7jzA2DzxWMZT.CiTvKAbmfKRNjgcmahBFu6w5cYH23YhJ";
//   const SUMSUB_SECRET_KEY = "h37JtAkgm3qAfccbNYk6iwHhzxc8H3Bj";

// Prod env
const SUMSUB_APP_TOKEN =
  "prd:jC4KDO5JFEJWhS9voKs4le7S.pbRqyc7Uw5bTJIGMCEwmtQPZ9UA7rcAf";
const SUMSUB_SECRET_KEY = "CmLNIVlMkqbgUkxzCnAbuKH6nyNPj8wx";

/** Create a payment link for
 * :product Any string as a product name
 * :amount in cents (like 200 for 2)
 * :currency like eur or gbp
 * Examples:
 * GET https://us-central1-nfttrx.cloudfunctions.net/v1/paymentlink/NFT%20purchase/10000/gbp
 * POST https://us-central1-nfttrx.cloudfunctions.net/v1/paymentlink/
 *  with args 	"product":"Idea NFT ID#123456"
 *		 		"amount":"200000"
 *		 		"currency":"eur"
 * Returns:
 * Stripe's payment link object
 * https://stripe.com/docs/api/payment_links/payment_links/object
 * contains property "url"
 */
//
//
app.all("/paymentlink/:product/:amount/:currency", async (req, res) => {
  let productName = req.params.product;
  let amount = +req.params.amount;
  let currency = req.params.currency;
  const stripeProduct = await stripe.products.create({
    name: productName,
  });
  const price = await stripe.prices.create({
    unit_amount: amount,
    currency: currency,
    product: stripeProduct.id,
  });

  const paymentLink = await stripe.paymentLinks.create({
    line_items: [
      {
        price: price.id,
        quantity: 1,
      },
    ],
  });
  res.send(paymentLink);
});

/** Create a payment link for
 * :product Any string as a product name
 * :amount in cents (like 200 for 2)
 * :currency like eur or gbp
 * Examples:
 * GET https://us-central1-nfttrx.cloudfunctions.net/v1/price/NFT%20purchase/10000/gbp
 * POST https://us-central1-nfttrx.cloudfunctions.net/v1/price/
 *  with args 	"product":"Idea NFT ID#123456"
 *		 		"amount":"200000"
 *		 		"currency":"eur"
 * Returns:
 * Stripe's payment price object
 * https://stripe.com/docs/api/prices/object
 */
//
//
app.all("/price/:product/:amount/:currency", async (req, res) => {
  let productName = req.params.product;
  let amount = +req.params.amount;
  let currency = req.params.currency;
  const stripeProduct = await stripe.products.create({
    name: productName,
  });
  const price = await stripe.prices.create({
    unit_amount: amount,
    currency: currency,
    product: stripeProduct.id,
  });
  res.send(price);
});

/**
 * Not sure how it supposed to work yet...
 * Example
 * POST https://us-central1-nfttrx.cloudfunctions.net/v1/paymentsession/price_1LFBfpDaK9yndacRkFXN2zeJ
 *
 */
app.post("/paymentsession/:priceid", async (req, res) => {
  let priceid = req.params.priceid;
  let success_url =
    req.body.success_url || `${req.headers.origin}/?success=true`;
  let cancel_url =
    req.body.cancel_url || `${req.headers.origin}/?canceled=true`;

  let metadata = { ...req.body };

  try {
    // Create Checkout Sessions from body params.
    const session = await stripe.checkout.sessions.create({
      line_items: [
        {
          price: priceid,
          quantity: 1,
        },
      ],
      mode: "payment",
      success_url: success_url,
      cancel_url: cancel_url,
      metadata: metadata,
    });
    res.redirect(303, session.url);
  } catch (err) {
    res.status(err.statusCode || 500).json(err.message);
  }
});

const changeItemsCount = async (Users, UserItems, metadata) => {
  const toUser = await Users.doc(metadata.toID)
    .get()
    .then((res) => res.data());
  const fromUser = await Users.doc(metadata.fromID)
    .get()
    .then((res) => res.data());
  Users.doc(metadata.toID).update({
    itemsCount: (+toUser?.itemsCount || 0) + 1,
  });
  Users.doc(metadata.fromID).update({
    itemsCount: (+fromUser?.itemsCount || 0) - 1,
  });
};

// GET https://us-central1-nfttrx.cloudfunctions.net/v1/stripehook
app.all("/stripehook", async (req, res) => {
  const todayAsTimestamp = firebase.firestore.Timestamp.now();
  console.log("Stripe webhook", req.method, todayAsTimestamp);
  let obj = {
    ...req.params,
    ...req.query,
    ...req.body,
    webhook_timestamp: todayAsTimestamp,
  };
  db.collection("Stripe").doc().set(obj);
  let metadata = obj.data.object.metadata;
  const Users = db.collection("Users");
  const UserItems = db.collection("UserItems");
  if (metadata && metadata.itemID) {
    UserItems.doc(metadata.itemID)
      .update({ ownerUid: metadata.toID, author: metadata.author })
      .then(() => {
        changeItemsCount(Users, UserItems, metadata);
        console.log(
          "Successfully made a record for " +
            metadata.itemID +
            " setting new owner to " +
            metadata.toID
        );
      })
      .catch((e) => {
        console.log(
          "Error for a record " +
            metadata.itemID +
            " and new owner " +
            metadata.toID
        );
      });
  } else {
    console.log("Metadata were incomplete", safeJsonStringify(metadata));
  }
  res.send(new Date() + "");
});

// GET https://us-central1-nfttrx.cloudfunctions.net/v1/get/collections/0/100
// GET https://us-central1-nfttrx.cloudfunctions.net/v1/get/assets/0/100
app.get("/get/:collection/:start/:limit", async (req, res) => {
  try {
    let r = {
      start: +req.params.start,
      collection: req.params.collection,
      limit: +req.params.limit,
      data: [],
    };
    let snap = await db
      .collection(r.collection)
      .limit(r.limit)
      .offset(r.start)
      //.orderBy(firebase.firestore.FieldPath.documentId())
      .get();
    snap.forEach((doc) => {
      r.data.push(doc.data());
    });
    res.json(r);
  } catch (e) {
    console.log(e);
    res.status(500).send(e.message);
  }
});

// GET https://us-central1-nfttrx.cloudfunctions.net/v1/everything
app.get(
  "/everything/:externalUserId/:ttlInSecs/:levelName",
  async (req, res) => {
    let externalUserId = req.params.externalUserId;
    let ttlInSecs = req.params.ttlInSecs;
    let levelName = req.params.levelName;

    let config = { method: "post", headers: { Accept: "application/json" } };
    config.url = `/resources/accessTokens?userId=${externalUserId}&ttlInSecs=${ttlInSecs}&levelName=${levelName}`;

    let ts = Math.floor(Date.now() / 1000);
    let signature = crypto.createHmac("sha256", SUMSUB_SECRET_KEY);
    signature.update(ts + config.method.toUpperCase() + config.url);

    config.headers["X-App-Access-Ts"] = ts;
    config.headers["X-App-Access-Sig"] = signature.digest("hex");
    config.headers["X-App-Token"] = SUMSUB_APP_TOKEN;
    config.data = "";
    config.url = SUMSUB_BASE_URL + config.url;

    console.log("everything", config);

    let response = await axios(config)
      .then(function (response) {
        console.log("Response:\n", safeJsonStringify(response));
        return response;
      })
      .catch(function (error) {
        console.log("Error:\n", error);
        return error;
      });
    if (response.data) {
      return res.json(response.data);
    }
    return res.send(safeJsonStringify(response));
  }
);

// GET https://us-central1-nfttrx.cloudfunctions.net/v1/agreement/pdf/3HJRHvjWiC8FMzLTSDGs
app.get("/agreement/:format/:docId", async (req, res) => {
  const { format, docId } = req.params;
  var additionalInfo = "";
  if ("pdf" != format && "html" != format && "both" != format) {
    return res.send("Format is not fupported: " + format);
  } else
    try {
      const docObj = await db
        .collection("docs")
        .doc(docId)
        .get()
        .then((doc) => {
          if (!doc.exists) return false;
          return doc.data();
        });
      if (!docObj) {
        return res.send("Document could not be found: " + docId);
      }
      additionalInfo += docObj.vars;
      let view = JSON.parse(docObj.vars);
      console.log("View", view);
      let html = mustache.render(docObj.template, view);
      let config = {
        format: "A4",
        directory: "/tmp",
        timeout: 540000, // in milliseconds
      };
      let docName = +new Date() + ".pdf";

      await pdf.create(html, config).toBuffer(async function (err, buffer) {
        console.log("PDF create: ", err);
        const bucket = getStorage().bucket("nfttrx.appspot.com");
        var file = bucket.file(docName);
        await file.save(buffer, (error) => {
          if (error) {
            console.log("PDF save error", error);
          }
        });
        await file.makePublic();
        const metaData = await file.getMetadata();
        const url = metaData[0].mediaLink;
        console.log("This is the url:", url);
        db.collection("docs").doc(docId).update({ pdf: url, html: html });
        if (format == "both") {
          res.send({ pdf: url, html: html });
        } else if (format == "html") {
          res.send(html);
        } else {
          res.send(url);
        }
      });
    } catch (ex) {
      return res.status(500).send("Exception: " + ex.message + additionalInfo);
    }
});

// GET https://us-central1-nfttrx.cloudfunctions.net/v1/pingCollections
app.get("/pingCollections", async (req, res) => {
  try {
    // rettrieve settings object
    let pingCollections = await db
      .collection("meta")
      .doc("pingCollections")
      .get()
      .then((doc) => {
        if (!doc.exists)
          return { limit: 20, count: 0, order_direction: "desc" };
        return doc.data();
      });

    let options = {
      offset: pingCollections.limit * pingCollections.count,
      limit: pingCollections.limit,
      format: "json",
    };

    let status = await axios
      .get("https://api.opensea.io/api/v1/collections", { params: options })
      .then((obj) => {
        return { ...obj.data, ...{ success: true } };
      })
      .catch((err) => {
        console.log("err", err);
        return { ...err, ...{ success: false } };
      });

    pingCollections.lastCall = status.success;

    if (status.success) {
      pingCollections.count++;
      //Object.keys(obj).forEach(k => (!obj[k] && obj[k] !== undefined) && delete obj[k]);

      pingCollections.lastRecordsLength = status.collections.length;

      if (pingCollections.lastRecordsLength) {
        const batch = db.batch();
        for (var i = status.collections.length - 1; i >= 0; i--) {
          let docObj = status.collections[i];
          let uid = "opensea-" + docObj.slug;
          docObj.uid = uid;
          batch.set(db.collection("collections").doc(uid), docObj);
        }
        // Commit the batch
        await batch.commit();
      }
    } else {
      let errObj = { ...status, ...pingCollections };
      pingCollections.lastError = safeJsonStringify(errObj);
    }
    await db.collection("meta").doc("pingCollections").set(pingCollections);
    res.send(safeJsonStringify(pingCollections));
  } catch (e) {
    console.log(e);
    res.status(500).send(e.message);
  }
});

// GET https://us-central1-nfttrx.cloudfunctions.net/v1/pingAssets
app.get("/pingAssets", async (req, res) => {
  try {
    // rettrieve settings object
    let pingAssets = await db
      .collection("meta")
      .doc("pingAssets")
      .get()
      .then((doc) => {
        if (!doc.exists)
          return { limit: 20, count: 0, order_direction: "desc" };
        return doc.data();
      });

    let options = {
      order_direction: pingAssets.order_direction,
      offset: pingAssets.limit * pingAssets.count,
      limit: pingAssets.limit,
      format: "json",
    };

    let status = await axios
      .get("https://api.opensea.io/api/v1/assets", { params: options })
      .then((obj) => {
        return { ...obj.data, ...{ success: true } };
      })
      .catch((err) => {
        console.log("err", err);
        return { ...err, ...{ success: false } };
      });

    pingAssets.lastCall = status.success;

    if (status.success) {
      pingAssets.count++;
      //Object.keys(obj).forEach(k => (!obj[k] && obj[k] !== undefined) && delete obj[k]);

      pingAssets.lastRecordsLength = status.assets.length;

      if (pingAssets.lastRecordsLength) {
        const batch = db.batch();
        for (var i = status.assets.length - 1; i >= 0; i--) {
          let docObj = status.assets[i];
          let uid = "opensea-" + docObj.id;
          docObj.uid = uid;
          batch.set(db.collection("assets").doc(uid), docObj);
        }
        // Commit the batch
        await batch.commit();
      }
    } else {
      let errObj = { ...status, ...pingAssets };
      pingAssets.lastError = safeJsonStringify(errObj);
    }
    await db.collection("meta").doc("pingAssets").set(pingAssets);
    res.send(safeJsonStringify(pingAssets));
  } catch (e) {
    console.log(e);
    res.status(500).send(e.message);
  }
});

const getSumSubUser = async (applicantId) => {
  let config = { method: "get", headers: { Accept: "application/json" } };
  config.url = `/resources/applicants/${applicantId}/one`;

  let ts = Math.floor(Date.now() / 1000);
  let signature = crypto.createHmac("sha256", SUMSUB_SECRET_KEY);
  signature.update(ts + config.method.toUpperCase() + config.url);

  config.headers["X-App-Access-Ts"] = ts;
  config.headers["X-App-Access-Sig"] = signature.digest("hex");
  config.headers["X-App-Token"] = SUMSUB_APP_TOKEN;
  config.data = "";
  config.url = SUMSUB_BASE_URL + config.url;

  console.log("everything", config);

  let response = await axios(config)
    .then(function (response) {
      console.log("Response:\n", safeJsonStringify(response));
      return response;
    })
    .catch(function (error) {
      console.log("Error:\n", error);
      return error;
    });
  return response.data;
};

app.get("/getSumsubUser/:applicantId", async (req, res) => {
  const applicatntId = req.params.applicantId;
  console.log("Applicant Id: ", applicatntId);
  const response = await getSumSubUser(applicatntId);
  const testObj = {
    response: response,
    testId: applicatntId,
  };
  return res.send(safeJsonStringify(testObj));
});

const getBlockPassUser = async (refId) => {
  const CLIENT_ID = "nfttrx";
  const config = {
    url: `https://kyc.blockpass.org/kyc/1.0/connect/${CLIENT_ID}/refId/${refId}`,
    method: "GET",
    headers: {
      Authorization: "9ed7c3fb08f783e31ad1c4c364342ffe",
    },
  };
  let response = await axios(config)
    .then(function (response) {
      console.log("Response:\n", safeJsonStringify(response));
      return response;
    })
    .catch(function (error) {
      console.log("Error:\n", error);
      return error;
    });
  return response.data;
};

// POST https://us-central1-nfttrx.cloudfunctions.net/v1/kycCallback
app.post("/kycCallback", async (req, res) => {
  console.log(req);
  try {
    const { params, body, query } = req;
    await db.collection("KYC").add({
      params,
      body,
      query,
    });
    // const data = await getSumSubUser(body.applicantId);
    const data = await getBlockPassUser(body.refId);

    if (data.data.status === "approved") {
      await db.collection("Users").doc(body.refId).update({
        blockpass: data,
        firstName: data.data.identities.family_name.value,
        lastName: data.data.identities.given_name.value,
        allRegisterStepsDone: true,
      });
    }

    res.send("OK!");
  } catch (e) {
    console.log(e);
    res.status(500).send(e.message);
  }
});

app.get(/.*/, (req, res) => {
  console.log(req);
  res.send("OK");
});

///---------- Open Account Fee functions ------
app.get("/public-key", (req, res) => {
  res.send({ publishableKey: stripe_pk });
});

app.get("/product-details", async (req, res) => {
  let data = await getProductDetails();
  res.send(data);
});

app.post("/create-payment-intent", async (req, res) => {
  const body = req.body;
  const productDetails = await getProductDetails();

  const options = {
    ...body,
    amount: productDetails.amount,
    currency: productDetails.currency,
  };

  try {
    const paymentIntent = await stripe.paymentIntents.create(options);
    res.json(paymentIntent);
  } catch (err) {
    res.json(err);
  }
});

let getProductDetails = async () => {
  const obj = await db
    .collection("meta")
    .doc("openAccountFee")
    .get()
    .then((doc) => {
      if (!doc.exists) return false;
      return doc.data();
    });
  return obj;
};

// Public APIs through Express: https://us-central1-nfttrx.cloudfunctions.net/v1
exports.v1 = functions.https.onRequest(app);
