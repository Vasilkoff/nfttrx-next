/**
* @author Maxim Vasilkov maxim@nfttrx.com
* @project: NFTTRX.com
* @date: 13th Jan 2022
*/


const firebase = require("firebase-admin");
const functions = require('firebase-functions');
const safeJsonStringify = require("safe-json-stringify");
const axios = require("axios");

const app = firebase.initializeApp({
  serviceAccount: "nfttrx-firebase-adminsdk-i5f5o-417e9646ec.json",
  databaseURL: "https://nfttrx-default-rtdb.europe-west1.firebasedatabase.app",
  storageBucket: "nfttrx.appspot.com",
});



// Public APIs which works through Express
const v1Apis = require("./v1Apis");
exports.v1 = v1Apis.v1;


// auth helper to replace https://nfttrx.firebaseapp.com/__/auth/action?mode=action&oobCode=code
//  example: https://nfttrx.firebaseapp.com/__/auth/action?mode=verifyEmail&oobCode=-LyCnONLcc_MNsa17AMdjxOgqrz4kZketb24JDKJY4MAAAGCF7dOtg&apiKey=AIzaSyA12AVUCkeFtpuVJLHmntowmSGxOWEQdfk&lang=en
// 
exports.auth = functions.https.onRequest((req, res) => {
  let action = req.query.mode;
  let code = req.query.oobCode;
  let apiKey = req.query.apiKey;
  let lang = req.query.lang;
  let i = req.url.indexOf('?');
  let query = req.url.substr(i+1);
  const base_url = "https://nfttrx.firebaseapp.com/__/auth/action?";
  const base_query = base_url+query;

  // Handle the user management action.
  switch (action) {
    case 'verifyEmail':
      axios.get(base_query)
      .then(function (response) {
        console.log('auth success',base_query);
        res.set('location', '/register?code='+code);
        //res.status(200).send(safeJsonStringify(response.data)+base_query);
        res.status(301).send(code);
      })
      .catch(function (error) {
        // handle error
        console.log('auth error',base_query,error);
        res.set('location', base_query);
        res.status(301).send(safeJsonStringify(error));
      })
      break;
    default:
      
      res.set('location', base_query);
      res.status(301).send(
        // `<!doctype html>
        //   <head>
        //     <title>NFTTRX</title>
        //   </head>
        //   <body>
        //     Could not perform: ${action}
        //     <br/>
        //     ${code}
        //   </body>
        // </html>`
      );
  }

  
  
});