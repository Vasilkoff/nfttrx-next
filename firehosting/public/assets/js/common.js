function bodyFixPosition() {
	setTimeout( function() {
		if (!document.body.hasAttribute('data-body-scroll-fix')) {
			var scrollPosition = window.pageYOffset || document.documentElement.scrollTop;
			document.body.setAttribute('data-body-scroll-fix', scrollPosition);
			document.body.style.overflow = 'hidden';
			document.body.style.position = 'fixed';
			document.body.style.top = '-' + scrollPosition + 'px';
			document.body.style.left = '0';
			document.body.style.width = '100%';
		}
	}, 15 ); 
}

function bodyUnfixPosition() {
	if (document.body.hasAttribute('data-body-scroll-fix')) {
		var scrollPosition = document.body.getAttribute('data-body-scroll-fix');
		document.body.removeAttribute('data-body-scroll-fix');
		document.body.style.overflow = '';
		document.body.style.position = '';
		document.body.style.top = '';
		document.body.style.left = '';
		document.body.style.width = '';
		window.scroll(0, scrollPosition);
	}
}

$(function () {

	//Vars
	var $doc = $(document);
	var $window = $(window);
	var $body = $('body');
	var $header = $('.header');

	//Open menu
	$doc.on('click', '.js-toggle-menu', function () {
		$body.toggleClass('menu-is-open');
		if ($body.hasClass('menu-is-open')) {
			bodyFixPosition();
		} else {
			bodyUnfixPosition();
		}
	});
	$(document).click(function(e){
		if ($(e.target).closest('.header-nav, .js-toggle-menu').length) return;
		$body.removeClass('menu-is-open');
		e.stopPropagation();
	});
	$('.header-nav ul li a').on('click', function () {
		bodyUnfixPosition();
		$body.removeClass('menu-is-open');
	});

	//Carousel
	var saleSlider = '.js-sale-carousel';
	if ($(saleSlider).length > 0) {
		var $saleSlider = new Swiper(saleSlider, {
			slidesPerView: 3,
			simulateTouch: false,
			loop: false,
			navigation: {
				nextEl: '.js-sale-carousel-next',
				prevEl: '.js-sale-carousel-prev',
			},
			breakpoints: {
				320: {
					slidesPerView: 1.08,
					spaceBetween: 12
				},
				768: {
					slidesPerView: 2,
					spaceBetween: 18,
				},
				1200: {
					slidesPerView: 3,
					spaceBetween: 18,
				}
			}
		});
	}

	// Popups
	$('.js-popup-btn').magnificPopup({
		callbacks: {
			beforeOpen: function() {
				bodyFixPosition();
			},
			beforeClose: function() {
				bodyUnfixPosition();
			}
		}
	});
	$('.js-popup-close').on('click', function() {
		$.magnificPopup.close();
	});

	//Select2
	function initSelect2() {
		var $select2 = $('.js-select-custom');
		if ($select2.length > 0) {
			$select2.select2({
				minimumResultsForSearch: Infinity
			}).on('change.select2', function(e) {
				var value = e.target.value;
				var $th = $(this);
				if (value) {
					$th.next().addClass('is-selected');
				} else {
					$th.next().removeClass('is-selected');
				}
			});
		}
	};

	initSelect2();

});
