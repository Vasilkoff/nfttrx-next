import { useDispatch, useSelector } from "react-redux";
import Header from "../components/common/Header.js";
import WalletPage from "../components/pages/wallet/WalletPage.js";
import WithAuth from "../utils/privateAuth.js";
import Router from "next/router";


export default function Profile() {
  const wallet = useSelector((state) => state.wallet.data);

  if (!wallet.length) {
    Router.push("/wallet/create-import")
  }
  return (
    <div className="wrapper">
      <Header />

      <WithAuth Component={WalletPage} />
    </div>
  )
}
