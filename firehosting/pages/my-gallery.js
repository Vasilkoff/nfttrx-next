import Header from '../components/common/Header'
import MyGalleryPage from '../components/pages/MyGalleryPage'
import WithAuth from '../utils/privateAuth'

export default function myGallery() {
  return (
    <div>
        <Header/>

        <WithAuth Component={MyGalleryPage}/>
    </div>
  )
}
