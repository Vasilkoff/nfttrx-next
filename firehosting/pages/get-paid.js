import Header from "../components/common/Header.js";
import GetPaidPage from "../components/pages/GetPaidPage.js";
import WithAuth from "../utils/privateAuth.js";

export default function ChangePassword() {
    return (
        <div className="wrapper">
            <Header />
            
            <WithAuth Component={GetPaidPage} />
        </div>
    )
}
