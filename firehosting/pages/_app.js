import { useEffect, useState } from "react";
import "../styles/main.css";
import "../styles/Template.css";
import { Provider, useDispatch, useSelector } from "react-redux";
import store from "../store/index.js";
import Head from "next/head";
import Preloader from "../components/common/preloader/Preloader";
import { MoralisProvider } from "react-moralis";
import { initializeRedux } from "../store/actions/initialize.js";
import Moralis from "moralis";
import { useMoralisWeb3Api } from "react-moralis";
import { Networks } from "../constants/wallet";
import { db } from "./../config/firebase";
import { state } from "./../constants/item";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";

const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_PRIVATE_KEY);

const APP_ID_ROPSTEN = process.env.REACT_APP_MORALIS_APPLICATION_ID;
const APP_ID = process.env.REACT_APP_MORALIS_APPLICATION_ID_PROD;

const SERVER_URL_ROPSTEN = process.env.REACT_APP_MORALIS_SERVER_URL;
const SERVER_URL = process.env.REACT_APP_MORALIS_SERVER_URL_PROD;

const GetNFTsComponentWrapper = ({ Component, pageProps }) => {
  const userData = useSelector((state) => state.auth?.data);
  const userName = userData?.user?.userName;
  const uid = userData?.uid;
  const user = userData?.user;
  const wallets = useSelector((state) => state.wallet.data);
  const Web3Api = useMoralisWeb3Api();
  const getNFTs = async (options) => {
    const nfts = await Web3Api.account.getNFTs(options);
    const result = nfts.result.map((token) => {
      const { image } = JSON.parse(token.metadata);
      const id = image.split("//")[1];
      const baseUrl = token.token_uri.split("/").splice(0, 4).join("/");
      const image_url = baseUrl + "/" + id;
      return {
        ...token,
        image_url: token.contract_type === "ERC721" ? image_url : image,
      };
    });
    return result;
  };
  const { keys, ...networks } = Networks;
  const networksArr = Object.keys(networks);
  const setCashedTime = () => {
    db.collection("Users").doc(uid).set(
      {
        cashedTime: Date.now(),
      },
      { merge: true }
    );
  };

  const getAllNfts = async () => {
    const arr = [];
    networksArr.forEach((el) => {
      wallets.forEach(async (wallet) => {
        const f = async () => {
          const res = await getNFTs({ chain: el, address: wallet.Address });
          return res.map((item) => ({
            ...item,
            blockchain: el,
            author: userName,
            ownerUid: uid,
            state: state.minted,
          }));
        };
        arr.push(f);
      });
    });
    
    let mintedNfts = [];
    for (let i = 0; i < arr.length; i++) {
      await new Promise((r) => setTimeout(r, 2000));
      const res = await arr[i]();
      mintedNfts = mintedNfts.concat(res);
    }
    const mintedNftsJson = JSON.stringify(mintedNfts);

    db.collection("Users").doc(uid).update({
      mintedNfts: mintedNftsJson,
    });
    setCashedTime();
  };

  const min60 = 60 * 60 * 1000;
  useEffect(() => {
    if (uid && !user?.cashedTime) {
      console.log("FIIIIIIIIIRST");
      getAllNfts();
    }

    if (uid && user?.cashedTime + min60 < Date.now()) {
      console.log("SEEEEECOND");
      getAllNfts();
    }
  }, [uid]);
  return <Component {...pageProps} />;
};

const ComponentsWrapper = (props) => {
  const [isRopsten, setIsRopsten] = useState(false);

  const dispatch = useDispatch();
  const isAuthLoading = useSelector((state) => state.auth.loading);

  useEffect(() => {
    if (window) {
      const host = window.location.hostname.split(".");
      const isTestNetwork = host.find(
        (item) => item === "test" || item === "localhost"
      );
      setIsRopsten(isTestNetwork);
      if (isTestNetwork) {
        Moralis.start({ appId: APP_ID_ROPSTEN, serverUrl: SERVER_URL_ROPSTEN });
      } else {
        Moralis.start({ appId: APP_ID, serverUrl: SERVER_URL });
      }
    }
    dispatch(initializeRedux());
  }, []);

  if (isAuthLoading) return <Preloader full />;
  if (!APP_ID || !SERVER_URL)
    throw new Error(
      "Missing Moralis Application ID or Server URL. Make sure to set your .env file."
    );

  if (isRopsten) {
    return (
      <MoralisProvider appId={APP_ID_ROPSTEN} serverUrl={SERVER_URL_ROPSTEN}>
        <GetNFTsComponentWrapper {...props} />
      </MoralisProvider>
    );
  }
  return (
    <MoralisProvider appId={APP_ID} serverUrl={SERVER_URL}>
      <GetNFTsComponentWrapper {...props} />
    </MoralisProvider>
  );
};

export default function MyApp(props) {
  if (typeof window === "object") {
    (function (u) {
      var s = document.createElement("script");
      s.async = true;
      s.src = u;
      var x = document.getElementsByTagName("script")[0];
      x.parentNode.insertBefore(s, x);
    })("https://widget.replain.cc/dist/client.js");
  }
  return (
    <>
      <Head>
        <link rel="stylesheet" href="/assets/css/main.min.css?v=20211007.1" />
        <link rel="stylesheet" href="/assets/css/margin.css" />
        <script type="text/javascript" src="/assets/js/replain.js"></script>
        <script src="https://cdn.blockpass.org/widget/scripts/release/3.0.2/blockpass-kyc-connect.prod.js"></script>
      </Head>
      <Elements stripe={stripePromise}>
        <Provider store={store}>
          <ComponentsWrapper {...props} />
        </Provider>
      </Elements>
    </>
  );
}
