import Footer from "../components/common/Footer.js";
import Header from "../components/common/Header.js";
import IpProtectionPage from "../components/pages/IpProtectionPage.js";

export default function Protection() {
    return (
        <div>
            <Header />
            
            <IpProtectionPage />

            <Footer/>
        </div>
    )
}
