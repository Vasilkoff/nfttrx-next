import Header from "../components/common/Header.js";
import Link from "next/link";
import Footer from "../components/common/Footer.js";

export default function () {
    return (
        <>
            <div className="wrapper404 container">
                <Header />
                <img alt="404" src="/assets/img/elements/404.png" />
                <p className="title-404">This page is lost.</p>
                <p className="description-404">
                    We've explored deep and wide,
                    <br />
                    but we can't find the page you were looking for.
                </p>
                <div className="screen__btns">
                    <Link href="/">
                        <a className="btn btn-round btn-fill-accent screen__buy">Back Home</a>
                    </Link>
                </div>
            </div>
            <Footer />
        </>
    )
}
