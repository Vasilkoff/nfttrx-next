import Header from "../components/common/Header";
import Footer from "../components/common/Footer";
import React from "react";
import TermsAndConditionsPage from "../components/pages/TermsAndConditionsPage";

export default function Home() {
    return (
        <div className="wrapper">
            <Header/>

            <TermsAndConditionsPage />

            <Footer/>
        </div>
    )
}
