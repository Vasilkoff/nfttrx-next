import Header from "../components/common/Header.js";
import ProfilePage from "../components/pages/ProfilePage.js";
import WithAuth from "../utils/privateAuth.js";

export default function Profile() {
  return (
    <div className="wrapper">
      <Header />

      <WithAuth Component={ProfilePage} />
    </div>
  );
}
