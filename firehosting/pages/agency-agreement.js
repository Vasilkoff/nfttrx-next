import Header from "../components/common/Header.js";
import AgencyAgreement from "../components/pages/agencyAgreement/AgencyAgreement.jsx";
import WithAuth from "../utils/privateAuth.js";

export default function Profile() {
  return (
    <div className="wrapper">
      <Header />

      <WithAuth Component={AgencyAgreement} />
    </div>
  );
}
