import Certificate from "../components/common/certificate/Certificate.jsx";
import CertificateForm from "../components/common/certificatForm/CertificateForm.js";
import Header from "../components/common/Header.js";

export default function ViewCertificate() {
    return <div>
        <Header />
        <div className="page container">
            <div className="row align-items-center">
                <div className="col-lg-6 col-12">
                    <CertificateForm view={true} />
                </div>
                <div className="col-lg-6 col-12 d-flex flex-column align-items-center">
                    <Certificate isSale />
                    <button style={{
                        margin: "20px auto"
                    }} className="btn btn-fill-accent btn-round">
                        Buy
                    </button>
                </div>
            </div>
        </div>
    </div>;
}
