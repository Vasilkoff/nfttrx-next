import Header from "../components/common/Header";
import Footer from "../components/common/Footer";
import React from "react";
import PrivacyPolicyPage from "../components/pages/PrivacyPolicyPage";

export default function Home() {
    return (
        <div className="wrapper">
            <Header/>

            <PrivacyPolicyPage />

            <Footer/>
        </div>
    )
}
