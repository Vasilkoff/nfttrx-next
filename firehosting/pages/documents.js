import Header from "../components/common/Header.js";
import DocumentsPage from "../components/pages/documents/DocumentsPage.js";
import WithAuth from "../utils/privateAuth.js";

export default function Profile() {
    return (
        <div className="wrapper">
            <Header />
            
            <WithAuth Component={DocumentsPage} />
        </div>
    )
}
