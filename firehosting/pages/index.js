import Header from "../components/common/Header";
import Footer from "../components/common/Footer";
import HomePage from "../components/pages/HomePage";

export default function Home() {
  return (
    <div className="wrapper">
      <Header />
      <HomePage />

      <Footer />
    </div>
  );
}
