import Header from "../../components/common/Header";
import WithAuth from "../../utils/privateAuth";
import PreviewPage from "../../components/pages/PreviewPage";

export default function AssetReview() {
  return (
    <div className="wrapper">
      <Header />
      <WithAuth Component={PreviewPage} />
    </div>
  )
}
