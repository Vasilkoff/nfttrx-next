import Header from "../components/common/Header.js";
import CreateNewItemPage from "../components/pages/CreateNewItemPage.js";
import WithAuth from "../utils/privateAuth.js";

export default function Protection() {
    return (
        <div>
            <Header />

            <WithAuth Component={CreateNewItemPage} />
        </div>
    )
}
