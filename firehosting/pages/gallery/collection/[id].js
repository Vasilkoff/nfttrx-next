import { useRouter } from 'next/router'
import Header from "../../../components/common/Header";
import Footer from "../../../components/common/Footer";



export default function CollectionPage() {
  const router = useRouter()
  const { id } = router.query;

  return (
  	<div className="wrapper">
        <Header/>
		    <div>
		    	<h1>Collection  Page {id}</h1>
		    </div>
        <Footer/>
    </div>
    )
}