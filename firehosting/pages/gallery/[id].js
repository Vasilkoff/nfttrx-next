import Header from "../../components/common/Header";
import Footer from "../../components/common/Footer";
import GalleryItem from '../../components/pages/GalleryItem';



export default function AssetPage() {
  

  return (
    <div className="wrapper">
        <Header/>
		    <GalleryItem />
        <Footer/>
    </div>
    )
}