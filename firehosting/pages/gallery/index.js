import Header from "../../components/common/Header";
import Footer from "../../components/common/Footer";
import React from "react";
import GalleryPage from "../../components/pages/GalleryPage";

export default function Home() {
    return (
        <div className="wrapper">
            <Header/>

            <GalleryPage />

            <Footer/>
        </div>
    )
}
