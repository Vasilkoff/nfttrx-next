import Header from "../../../components/common/Header";
import Footer from "../../../components/common/Footer";
import React from "react";
import UsersPage from "../../../components/pages/users/Users";

export default function Home() {
    return (
        <div className="wrapper">
            <Header/>

            <UsersPage />

            <Footer/>
        </div>
    )
}
