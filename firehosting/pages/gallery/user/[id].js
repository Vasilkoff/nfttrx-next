import Header from "../../../components/common/Header";
import Footer from "../../../components/common/Footer";
import UserPage from "../../../components/pages/UserPage";


export default function User() {
  return (
    <div className="wrapper">
      <Header />

      <UserPage />

      <Footer />
    </div>
  );
}
