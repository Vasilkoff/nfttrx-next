import Header from "../components/common/Header";
import React from "react";
import LoginPage from "../components/pages/LoginPage.js";
import WithNotAuth from "../utils/publishAuth.js";
import Footer from "../components/common/Footer.js";

export default function Home() {
    return (
        <div className="wrapper">
            <Header/>
            <WithNotAuth Component={LoginPage} />
            <Footer />
        </div>
    )
}
