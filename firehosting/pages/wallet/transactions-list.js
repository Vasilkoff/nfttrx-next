import Header from "../../components/common/Header";
import TransactionListPage from "../../components/pages/wallet/TransactionsListPage";
import WithAuth from "../../utils/privateAuth";

export default function TransactionsList() {
  return (
    <div className="wrapper">
      <Header />

      <WithAuth Component={TransactionListPage} />
    </div>
  );
}
