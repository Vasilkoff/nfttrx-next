import QrPage from "../../components/pages/wallet/QrPage";
import Header from "../../components/common/Header";
import WithAuth from "../../utils/privateAuth";

export default function QR() {
  return (
    <div className="wrapper">
      <Header />

      <WithAuth Component={QrPage} />
    </div>
  );
}
