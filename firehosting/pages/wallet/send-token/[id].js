import Header from "../../../components/common/Header.js";
import WithAuth from "../../../utils/privateAuth.js";
import SendPage from "../../../components/pages/wallet/SendPage.js";

export default function Create() {
    return (
        <div className="wrapper">
            <Header />
            <WithAuth Component={() => <SendPage isToken />} />
        </div>
    )
}
