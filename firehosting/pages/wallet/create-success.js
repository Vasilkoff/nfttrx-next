import Router from "next/router";
import { useEffect } from "react";
import Header from "../../components/common/Header.js";
import SuccessCreatePage from "../../components/pages/wallet/SuccessCreatePage.js";
import WithAuth from "../../utils/privateAuth.js";

export default function Import() {

  useEffect(() => {
    setTimeout(() => Router.push("/wallet"), 7000);
  }, []);

  return (
    <div className="wrapper">
      <Header />
      <WithAuth Component={SuccessCreatePage} />
    </div>
  )
}
