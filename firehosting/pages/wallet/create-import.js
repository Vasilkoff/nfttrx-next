import { useSelector } from "react-redux";
import Header from "../../components/common/Header.js";
import WithAuth from "../../utils/privateAuth.js";
import Router from "next/router";
import CreateImportPage from "../../components/pages/wallet/CreateImportPage.js";

export default function Profile() {
    const isWallet = useSelector((state) => state.wallet.data);
    
    if(isWallet.length) {
        Router.push("/wallet")
    }
    return (
        <div className="wrapper">
            <Header />
            <WithAuth Component={CreateImportPage} />
        </div>
    )
}
