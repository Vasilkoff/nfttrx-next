import Header from "../../../components/common/Header";
import TransactionPage from "../../../components/pages/wallet/TransactionPage";
import WithAuth from "../../../utils/privateAuth";

export default function Transaction() {
  return (
    <div className="wrapper">
      <Header />

      <WithAuth Component={TransactionPage} />
    </div>
  );
}
