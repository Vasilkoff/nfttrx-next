import { useSelector } from "react-redux";
import Header from "../../components/common/Header.js";
import WithAuth from "../../utils/privateAuth.js";
import Router from "next/router";
import ImportWalletPage from "../../components/pages/wallet/ImportWalletPage.js";

export default function Import() {
    const isWallet = useSelector((state) => state.auth.data?.user?.wallet);

    if(isWallet) {
        return Router.push("/wallet")
    }
    return (
        <div className="wrapper">
            <Header />
            <WithAuth Component={ImportWalletPage} />
        </div>
    )
}
