import { useSelector } from "react-redux";
import Header from "../../components/common/Header.js";
import WithAuth from "../../utils/privateAuth.js";
import Router from "next/router";
import CreateWalletPage from "../../components/pages/wallet/CreatetWalletPage.js";

export default function Create() {
    const isWallet = useSelector((state) => state.auth.data?.user?.wallet);

    if(isWallet) {
        return Router.push("/wallet")
    }
    return (
        <div className="wrapper">
            <Header />
            <WithAuth Component={CreateWalletPage} />
        </div>
    )
}
