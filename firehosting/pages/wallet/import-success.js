import Router from "next/router";
import { useEffect } from "react";
import Header from "../../components/common/Header.js";
import SuccessImportPage from "../../components/pages/wallet/SuccessImportPage.js";
import WithAuth from "../../utils/privateAuth.js";

export default function Import() {

  useEffect(() => {
    setTimeout(() => Router.push("/wallet"), 7000);
  }, []);

  return (
    <div className="wrapper">
      <Header />
      <WithAuth Component={SuccessImportPage} />
    </div>
  )
}
