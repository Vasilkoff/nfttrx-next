import Header from "../components/common/Header.js";
import ChangePasswordPage from "../components/pages/ChangePasswordPage.js";
import WithAuth from "../utils/privateAuth.js";

export default function ChangePassword() {
    return (
        <div className="wrapper">
            <Header />
            
            <WithAuth Component={ChangePasswordPage} />
        </div>
    )
}
