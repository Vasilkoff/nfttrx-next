import React, { useEffect, useState } from "react";
import fire from "../../config/firebase.js";
import css from "../../styles/auth.module.css";
import { useSelector } from "react-redux";
import { getAuth } from "../../store/selectors.js";

const VerifyAccaunt = () => {
  const [error, setError] = useState("");
  const [seconds, setSeconds] = useState(59);
  const [sending, setSending] = useState(false);
  const user = useSelector(getAuth);

  useEffect(() => {
    let id;
    if (!sending) {
      id = setInterval(() => {
        setSeconds((prev) => {
          if (prev > 1) {
            return prev - 1;
          }
          return 0;
        });
      }, 1000);
    }
    return () => {
      clearInterval(id);
    };
  }, [sending]);

  const send = async (e) => {
    setSending(true);
    setSeconds(59);
    const res = fire.auth().currentUser;
    res
      .sendEmailVerification()
      .then((res) => {
        console.log(res);
        setSending(false);
      })
      .catch((error) => {
        setError(error.message)
      });
  };
  return (
    <div className={css.wrapper + " col-12 col-md-6"}>
      <p className={css.welocome}>Step 2</p>
      <h3 className={css.header}>Email confirmation</h3>
      <p className={css.description}>
        An email verification link has been sent to {user.email}
      </p>
      <img
        className={css.icon}
        src="./assets/img/elements/email-confirm-send.svg"
        alt="Email verfication icon"
      />
      <button
        disabled={seconds > 0}
        onClick={send}
        className="btn w-100 btn-fill-accent main-btn-radius"
      >
        Send confirmation code
      </button>
      <div className={css.seconds}>
        {seconds > 0 ? (
          <p className={css.description}>
            Send confirmation code after 00:
            {seconds < 10 ? "0" + seconds : seconds}
          </p>
        ) : (
          <p className={css.description}>
            Now you can send confirmation code again!
          </p>
        )}
      </div>
    </div>
  );
};

export default VerifyAccaunt;
