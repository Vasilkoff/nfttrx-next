import Aside from "./Aside.js";
import WalletCard from "./walletCard/WalletCard.js";
import WalletList from "./walletList/WalletList.js";

export default function WalletWrapper({children}) {
  return (
    <section className="section page profile h-100vh">
      <div className="container-big">
        <div className="profile-row d-flex flex-wrap">
          <Aside />
          <div className="profile-content">
            <div className="col-lg-11 col-xl-9 col-12 w-100">
              {children}
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
