import ProfileAsidePaths from "../../constants/profileAside.js";
import { NavLink } from "./Navlink.js";
import firebase from '../../config/firebase.js';
import Link from "next/link";
import { useDispatch } from "react-redux";
import { setLogout } from "../../store/actions/authAction.js";

export default function Aside() {
  const dispatch = useDispatch();

  const logout = () => {
    firebase.auth().signOut();
    dispatch(setLogout())
  }

  return (
    <aside className="profile-sidebar col-sm-12 col-md">
      <div className="label-color profile-sidebar__title">ACCOUNT SETTINGS</div>
      <nav className="profile-nav">
        <ul>
          {
            ProfileAsidePaths.map((props) => (
              <li key={props.title}>
                <NavLink className="link link-icon" {...props}>
                  {props.icon || <img alt={props.title} src={props.path} />}
                  <span className="link__title">{props.title}</span>
                </NavLink>
              </li>
            ))
          }
          <li>
            <Link href="/">
              <a onClick={logout} className="link link-icon">
                <img src="/assets/img/icons/log-out.svg" alt="Logout" /> 
                <span className="link__title">Logout</span>
              </a>
            </Link>
          </li>
        </ul>
      </nav>
    </aside>
  )
}
