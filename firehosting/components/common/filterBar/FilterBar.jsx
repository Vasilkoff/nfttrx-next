function FilterBar() {
  return (
    <div className="gallery-top page-row d-flex flex-wrap">
      <div className="page-col-4">
        <form className="search gallery-search">
          <label className="input input-little">
            <input
              type="text"
              name="q"
              className="input__field input__field-gray-next input__field-round search__field"
              placeholder="Search"
            />
            <button className="search__btn">&nbsp;</button>
          </label>
        </form>
      </div>
      <div className="page-col-4">
        <form className="gallery-form">
          <label className="input input-little gallery-form__item">
            <input
              type="text"
              name="price_min"
              className="input__field input__field-gray-next input__field-round"
              placeholder="Price Min"
            />
          </label>
          <label className="input input-little gallery-form__item">
            <input
              type="text"
              name="price_max"
              className="input__field input__field-gray-next input__field-round"
              placeholder="Price Max"
            />
          </label>
          <button
            className="btn btn-tiny btn-fill-accent btn-round gallery-form__btn"
            disabled
          >
            Apply
          </button>
        </form>
      </div>
      <div className="page-col-4">
        <label className="input input-little gallery-form__select">
          <select
            name="price_status"
            className="input__field input__field-gray-next input__field-round js-select-custom"
          >
            <option disabled selected>
              Status
            </option>
            <option value="1">Status 1</option>
            <option value="1">Status 2</option>
            <option value="1">Status 3</option>
          </select>
        </label>
      </div>
      <div className="page-col-4">
        <label className="select input input-little gallery-form__select">
          <span className="select__title">Price:</span>
          <select
            name="price_status"
            className="input__field input__field-gray-next input__field-round js-select-custom"
          >
            <option value="1">Low to High</option>
            <option value="1">Price sort 2</option>
          </select>
        </label>
      </div>
    </div>
  );
}

export default FilterBar;
