import css from "./DocumentPdf.module.css";

function DocumentPdf({ link, text }) {
  return (
    <a href={link} className={css.wrapper}>
      <div className={css.body}>
        <img src="/assets/img/elements/doc-screen.jpeg" alt="Screen" />
        {/* <iframe src={link} frameborder="0"></iframe> */}
      </div>
      <div className={css.footer}>
        <p>{text}</p>
        <img src="/assets/img/icons/dowload.png" alt="Dowload" />
      </div>
    </a>
  );
}

export default DocumentPdf;
