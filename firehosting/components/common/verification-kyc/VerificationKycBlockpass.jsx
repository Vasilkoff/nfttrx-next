import { useEffect } from "react";
import { useSelector } from "react-redux";
import { db } from "../../../config/firebase";
import { getAuth } from "../../../store/selectors";
import css from "../../../styles/auth.module.css";

export default function VerificationKycBlockpass() {
  const { uid, user } = useSelector(getAuth);

  const cancel = () => {
    db.collection("Users")
    .doc(uid)
    .update({ kycVerificationCanceled: true })
    .then(() => {
      window.location.reload();
    });
  };
 
  const loadBlockpassWidget = () => {
    const blockpass = new window.BlockpassKYCConnect(
      "nfttrx", // service client_id from the admin console
      {
        refId: uid, // assign the local user_id of the connected user
      }
    );

    blockpass.startKYCConnect();

    blockpass.on("KYCConnectSuccess", () => {
      //add code that will trigger when data have been sent.
    });
  };
  useEffect(() => {
    loadBlockpassWidget();
    db.collection("Users").doc(uid).onSnapshot((doc) => {
      if(!user?.allRegisterStepsDone && doc.data()?.allRegisterStepsDone) {
        window.location.reload()
      }
    })
  }, []);

  return (
    <div className={css.wrapper + " col-12 col-md-6"}>
      <p className={css.welocome}>Step 3</p>
      <h3 className={css.header}>Verify Identity</h3>
      <p className="mb-5">Verify your identity to protect your account.</p>
      <div className={css.btnsWrapper}>
        <button onClick={cancel} type="button" className="btn">
          Skip
        </button>
        <button
          className={"btn btn-fill-accent main-btn-radius " + css.kyc}
          id="blockpass-kyc-connect"
        >
          Verify with Blockpass
        </button>
      </div>
    </div>
  );
}
