// import snsWebSdk from "@sumsub/websdk";
// import { useState, useEffect } from "react";
// import { useSelector } from "react-redux";
// import api from "../../../api/api";
// import { db } from "../../../config/firebase";
// import { getAuth } from "../../../store/selectors";
// import css from "../../../styles/auth.module.css";
// // 
// // 
// // 
// // 
// // 
// //  NOT USED Coomponent
// //  
// //  But for the case if we deside to use sumsub KYC verification 
// //  Lets leave it here for the future
// // 
// // 
// // 

// async function getNewAccessToken(uid) {
//   const sec = 600;
//   return await api.getNewToken(uid, sec);
// }

// function launchWebSdk(accessToken, uid) {
//   let snsWebSdkInstance = snsWebSdk
//     .init(accessToken, function () {
//       return getNewAccessToken(uid);
//     })
//     .withConf({
//       lang: "en",
//       onMessage: (type, payload) => {
//         console.log("WebSDK onMessage", type, payload);
//       },
//       onError: (error) => {
//         console.error("WebSDK onError", error);
//       },
//     })
//     .withOptions({ addViewportTag: false, adaptIframeHeight: true })
//     .on("stepCompleted", (payload) => {
//       console.log("stepCompleted", payload);
//     })
//     .on("onError", (error) => {
//       console.log("onError", error);
//     })
//     .onMessage((type, payload) => {
//       console.log("onMessage", type, payload);
//       if (type === "idCheck.applicantStatus") {
//         const allRegisterStepsDone = payload.reviewStatus === "completed";
//         db.collection("Users")
//           .doc(uid)
//           .set({ verification: payload, allRegisterStepsDone }, { merge: true })
//           .finally(() => {
//             window.location.reload();
//           })
//       }
//     })
//     .build();
//   snsWebSdkInstance.launch("#sumsub-websdk-container");
// }

// export default function VerificationKyc() {
//   const [error, setError] = useState("");
//   const { uid } = useSelector(getAuth);

//   const getKycVerfication = async () => {
//     const res = await getNewAccessToken(uid);
//     if (res.data.token) {
//       launchWebSdk(res.data.token, uid);
//     } else {
//       console.error(res);
//       setError(
//         "Something went wrong with your KYC verification code. Please try later or connect with our suppirt."
//       );
//     }
//   };

//   useEffect(() => {
//     setTimeout(() => {
//         getKycVerfication();
//     }, 3000)
//   }, []);

//   return (
//     <div className={css.wrapper + " col-12 col-md-6"}>
//       <p className={css.welocome}>Step 4</p>
//       <h3 className={css.header}>Verify Identity</h3>
//       <p>Verify your identity to protect your account.</p>
//       <div id="sumsub-websdk-container"></div>
//     </div>
//   );
// }
