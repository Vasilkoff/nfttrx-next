import React, { useState } from "react";
import fire from "../../config/firebase.js";
import css from "../../styles/auth.module.css";
import Checkbox from "./Checkbox.js";
import Input from "./Input.js";
import { useRouter } from "next/router";
import Link from "next/link";

const Register = () => {
  const router = useRouter();
  const [error, setError] = useState("");
  const [sending, setSending] = useState(false);

  const submit = async (e) => {
    e.preventDefault();
    const { email, password, rePassword } = e.target.elements;
    if (password.value === rePassword.value) {
      setSending(true);
      setError("");
      try {
        fire
          ?.auth()
          .createUserWithEmailAndPassword(email.value, password.value)
          .then(({ user }) => {
            console.log(user);
            user.sendEmailVerification();
          });
      } catch (error) {
        setSending(false);
        console.log(error);
        setError(error.message);
      }
    } else {
      setError("Password mismatch");
    }
  };
  return (
    <div className={css.wrapper + " col-12 col-md-6"}>
      <p className={css.welocome}>Welcome!</p>
      <h3 className={css.header}>Create to your account</h3>
      <form onSubmit={submit}>
        <Input
          name="email"
          title="Email"
          type="email"
          placeholder="Email"
          required
        />
        <Input
          name="password"
          title="Created Password"
          type="password"
          placeholder="Created Password"
          minLength={8}
          required
        />
        <Input
          name="rePassword"
          title="Repeat Password"
          type="password"
          placeholder="Repeat Password"
          minLength={8}
          required
        />
        <div className={css.remember + " " + css.registerRemember}>
          <Checkbox required title="I agree to Terms of Use" />
        </div>
        <button
          disabled={sending}
          type="submit"
          className="btn w-100 btn-fill-accent main-btn-radius"
        >
          Continue
        </button>
        <div className={css.account}>
          You have an account?
          <Link href="/login">
            <a>Login in</a>
          </Link>
        </div>
        {error && (
          <div className="error-message">
            <img src="/assets/img/icons/alert-circle.svg" />
            {error.replace("Firebase:", "")}
          </div>
        )}
      </form>
    </div>
  );
};

export default Register;
