import Head from "next/head";
import Link from 'next/link';
import { useDispatch, useSelector } from 'react-redux';

import css from "../../styles/wallet.module.css";
import ProfileAsidePaths from "../../constants/profileAside.js";
import { useRouter } from "next/router";
import { useRef, useState } from "react";
import { useOutsideClick } from "../../hooks/outsideClick.js"
import fire from "../../config/firebase.js";
import { setLogout } from "../../store/actions/authAction";

export default function Header() {
  const modalRef = useRef();
  const auth = useSelector(state => state.auth.data);
  const [isModal, setIsModal] = useState(false);
  const dispatch = useDispatch()
  useOutsideClick(modalRef, () => setIsModal(false));

  const logout = () => {
    fire.auth().signOut();
    dispatch(setLogout());
  }

  const { pathname } = useRouter();

  const modal = <div className={css.block + ' ' + (isModal && css.activeModal) + ' ' + css.headerModal}>
    <div className={css.corner + " " + css.left}></div>
    {
      ProfileAsidePaths?.map(({ href, icon, path, title }) => <Link key={href} href={href}>
        <a className={`${css.acc} ${pathname === href && css.activeWallet}`}>
          <div className={css.wallet__item}>
            {icon || <img alt="Wallet" src={path} />}
            {title}
          </div>
        </a>
      </Link>
      )
    }
    <Link href="/">
      <a onClick={logout} className={css.acc} >
        <div className={css.wallet__item}>
          <img src="/assets/img/icons/log-out.svg" alt="Logout" />
          Logout
        </div>
      </a>
    </Link>
  </div>


  return (
    <div>
      <Head>
        <meta charSet="utf-8" />

        <title>NFT marketplace</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png" />
        <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon.png" />
        <link rel="manifest" href="/assets/favicon/site.webmanifest" />
        <link rel="mask-icon" href="/assets/favicon/safari-pinned-tab.svg" color="#000000" />

        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta name="theme-color" content="#ffffff" />

        <link rel="stylesheet" href="/assets/css/main.min.css?v=20211007.1" />
      </Head>

      <header className="header js-header-fixed">
        <div className="container">
          <div className="row align-items-center justify-content-between">
            <div className="col-auto">
              <Link href="/">
                <a className="logo header-logo">
                  <img src="/assets/img/logo.svg" alt="NFTTRX" className="logo__img" />
                </a>
              </Link>
            </div>
            <div className="col-auto">
              <nav className="header-nav">
                <ul>
                  <li>
                    <Link href="/#sale">
                      <a>Sale</a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/gallery">
                      <a>Gallery</a>
                    </Link>
                  </li>

                  {
                    auth
                      ? <>
                        <div ref={modalRef} onClick={() => setIsModal(!isModal)} className="profile">
                          <img src={auth?.user?.avatar || "/assets/img/icons/avatars.svg"} alt="profile" />
                          {modal}
                        </div>
                      </>
                      : <Link href="/login">
                        <a>
                          <button className='btn btn-solid-accent main-btn-radius'>
                            Get in
                          </button>
                        </a>
                      </Link>
                  }
                  <Link href="/protection">
                    <a>
                      <button className='btn btn-fill-accent main-btn-radius protection'>
                        IP protection
                      </button>
                    </a>
                  </Link>

                  <li>
                    <Link href="/#faq">
                      <a>FAQs</a>
                    </Link>
                  </li>
                </ul>
              </nav>
              <button className="d-md-none sandwich header-sandwich js-toggle-menu">
                <span className="sandwich__inner">&nbsp;</span>
              </button>
            </div>
          </div>
        </div>
      </header>
    </div>
  );
}
