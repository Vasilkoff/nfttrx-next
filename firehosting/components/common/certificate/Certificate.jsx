import css from "./Certificate.module.css"

export default function Certificate({ isSale, isSmall, name }) {

  return <div className={`${css.certificate} ${isSmall && css.noShadow} row`}>
    <div className={`${css.left} ${isSmall && css.small} col-8`}>
      <h1 className={`${css.backText}`}>NFTTRX</h1>
      <h2 className={`${css.mainTitle}`}>CERTIFICATE</h2>
      <h3 className={css.description}>This is to certify that</h3>
      <div className={css.name}>{name || "Simon Papazov"}</div>
      <div className={`${css.line}`} />
      <div className={`${css.title} ${css.spaceXX}`}>The owner of public key</div>
      <div className={css.code}>0xa9f801f160fe6a866dd3404599350abbcaa95274</div>
      <div className={`${css.line}`} />
      <div className={css.title}>crypto wallet on Tron public main net blockchain</div>
      <div className={`${css.title} ${css.spaceXX}`}>Made a translation</div>
      <div className={css.code}>0x99353fc2bd3febe299a03081cade75eb9f36de2a4ff0a889f03d26c605e4aff7</div>
      <div className={`${css.line} w-100`} />
      <div className={`${css.idea} ${css.spaceXX}`}>“Title of the Idea”</div>
      <div className={`${css.line}`} />
      <div className={css.title}>Short description of the idea specified by the minter</div>
      {
        !isSmall && <>
          <div className={`${css.title} ${css.spaceXX} ${css.upper}`}>NFT has the hidden attachment</div>
          <div className={`${css.line} ${css.blue}`} />
          <div className="d-flex align-items-center">
            <div className={css.title}>DATE </div>
            <div className={`${css.title} ${css.black} ${css.mlXX}`}>1</div>
            <div className={css.verticalLine} />
            <div className={`${css.title} ${css.black}`}>OCTOBOR</div>
            <div className={css.verticalLine} />
            <div className={`${css.title} ${css.black}`}>2021</div>
            <div className={css.verticalLine} />
            <div className={`${css.title} ${css.black}`}>10:10:35 PM +UTC  </div>
          </div>
          <div className={`${css.line} ${css.blue}`} />
        </>
      }
    </div>
    <div className={`${css.right} ${isSmall && css.rightSmall} col-4`}>
      <img src="/assets/img/elements/white-logo.png" alt="NFTTRX" />
      {
        isSale && <div className={css.protected}>
          <img src="/assets/img/elements/protected-price.png" />
          <div className={css.price}>$ 9,99</div>
        </div>
      }

    </div>
  </div>;
}
