import React, { useState } from 'react';
import css from "../../styles/auth.module.css";
import Checkbox from "./Checkbox.js";
import Input from "./Input.js";
import Link from "next/link";
import useInput from "../../hooks/useInput.js";
import firebase from "../../config/firebase.js";

const Login = ({ }) => {
	const [error, setError] = useState("");
	const [sending, setSending] = useState(false);

	const email = useInput("");
	const password = useInput("");
	const remember = useInput(false);

	const forgotPassword = () => {
		firebase.auth().sendPasswordResetEmail(email.value)
			// .then(() => {
			// 	alert("Message was send to " + email.value)
			// })
	}

	const submit = async (e) => {
		e.preventDefault();
		setError("");
		setSending(true);
		try {
			await firebase.auth().setPersistence(
				remember
					? firebase.auth.Auth.Persistence.LOCAL
					: firebase.auth.Auth.Persistence.SESSION
			)
				.then(async () => {
					try {
						await firebase
							?.auth()
							.signInWithEmailAndPassword(email.value, password.value);
					} catch (error) {
						setSending(false);
						setError(error.message);
					}
				})
				.catch((error) => {
					setSending(false);
					setError(error.message);
				});
		} catch (error) {
			setSending(false);
			setError(error.message);
		}
	}

	return (
		<div className={css.wrapper + " col-md-6 col-12"}>
			<p className={css.welocome}>Welcome back</p>
			<h3 className={css.header} >Login to your account</h3>
			<form onSubmit={submit}>
				<Input
					{...email}
					name="email"
					title="Email"
					type="email"
					placeholder="Email"
					required
				/>
				<Input
					{...password}
					name="password"
					title="Password"
					type="password"
					placeholder="Password"
					minLength={8}
					required
				/>
				<div className={css.remember}>
					<Checkbox {...remember} name="remember" title="Remember me" />
					<Link href="#">
						<a onClick={forgotPassword} className={css.forgotPassword}>Forgot password?</a>
					</Link>
				</div>
				<button
					type="submit"
					disabled={sending}
					className='btn w-100 btn-fill-accent main-btn-radius'
				>
					Login now
				</button>
				<div className={css.account}>
					Dont have an account? 
					<Link href="/register">
						<a>Sign up now</a>
					</Link>
				</div>
				{error && <div className='error-message'>
					<img src="/assets/img/icons/alert-circle.svg" />
					{error.replace("Firebase:", "")}
				</div>}
			</form>
		</div>
	)
}

export default Login;
