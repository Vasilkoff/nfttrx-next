import React, { useEffect, useRef } from "react";
import { useState } from "react";
import css from "../../../styles/wallet.module.css";
import Select from "react-select";
import { Networks } from "../../../constants/wallet.js";
import { useDispatch, useSelector } from "react-redux";
import {
  setNetwork,
  setSelectedWallet,
} from "../../../store/actions/walletAction.js";
import Link from "next/link";
import { useOutsideClick } from "../../../hooks/outsideClick.js";
import { useMoralis } from "react-moralis";
import { ethers } from "ethers";

export const CustomLabel = ({ network, Networks }) => {
  return (
    <div className="d-flex">
      {Networks && Networks[network]?.image && (
        <img
          width="27px"
          src={Networks[network].image}
          alt={network}
          className="mr-2"
        />
      )}
      {network?.toUpperCase() + (Networks ? " Network" : "")}
    </div>
  );
};

export default React.memo(function () {
  const modalRef = useRef();
  const [opened, setOpened] = useState(false);
  const wallets = useSelector((state) => state.wallet?.data);
  const selectedWalletId = useSelector(
    (state) => state.wallet?.selectedWallet?.id
  );
  const { keys, ...net } = Networks;
  const networkKeys = Object.keys(net);
  const dispatch = useDispatch();
  const { authenticate, isAuthenticated, logout, user } = useMoralis();
  console.log(user, user?.get("authData")?.moralisEth);

  useOutsideClick(modalRef, () => setOpened(false));

  const open = () => {
    setOpened(!opened);
  };

  const handleWallet = (id) => {
    const selectedWallet = wallets.find((item) => item.id === id);
    dispatch(setSelectedWallet(selectedWallet));
    setOpened(false);
  };

  const onMetaMaskAuth = () => {
    authenticate({ provider: "metamask" });
  };

  const onMint = async () => {
    console.log("start");
    // const wallet = new ethers.Wallet(, Provider(selectedNetwork));
    const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
    const signer = provider.getSigner();
    console.log(signer);
    // const signature = await signer.getAddress(); 
    // console.log(signature);

    const abi = [
      "function mint(address address, hash uint256) public return uint256"
    ]
    const contract = new ethers.Contract("0x4df448941f331fa89eeb4494552622a0245a180c", abi, signer)
    console.log(contract);
    const res = await contract.mint("0x9fb6CAAdB23762098f21d57c4f5fc07b7F7D8A25")
    console.log(res);
  }

  const options = networkKeys?.map((network) => ({
    value: network,
    label: <CustomLabel network={network} Networks={Networks} />,
  }));

  useEffect(() => {
    dispatch(setNetwork(networkKeys[0]));
  }, []);

  return (
    <div className="d-flex justify-content-between mb-2">
      <Select
        onChange={(data) => {
          dispatch(setNetwork(data.value));
        }}
        options={options}
        defaultValue={options[0]}
        className={`col-6 ${css.select}`}
      />
      <div ref={modalRef} className={css.wrapper}>
        <div onClick={open} className={css.burger}>
          <div className={css.burgerUnder}>
            <div className={css.line + " " + (opened && css.line1)} />
          </div>
        </div>
        <div className={css.block + " " + (opened && css.activeModal)}>
          <div className={css.corner}></div>
          {wallets?.map(({ name, id }) => (
            <div
              onClick={() => handleWallet(id)}
              key={name}
              className={`${css.acc} ${
                id === selectedWalletId && css.activeWallet
              }`}
            >
              <div className={css.wallet__item}>
                <img
                  alt="Wallet"
                  src={`/assets/img/icons/${
                    id === selectedWalletId
                      ? "selected-wallet.png"
                      : "wallet.svg"
                  }`}
                />
                {name}
              </div>
              {id === selectedWalletId ? (
                <img
                  alt="Wallet"
                  width="18px"
                  src="/assets/img/icons/selected.svg"
                />
              ) : null}
            </div>
          ))}
          <Link href="/wallet/export">
            <a className={css.acc}>Export Account</a>
          </Link>
          <Link href="/wallet/import">
            <a className={css.acc}>Import Account</a>
          </Link>
          <Link href="/wallet/create">
            <a className={css.acc}>Create Account</a>
          </Link>
          <a onClick={onMetaMaskAuth} className={css.acc}>
            Auth Metamask
          </a>
          {
            isAuthenticated && <a onClick={onMint} className={css.acc}>
              MINT NFT
            </a>
          }
        </div>
      </div>
    </div>
  );
})
