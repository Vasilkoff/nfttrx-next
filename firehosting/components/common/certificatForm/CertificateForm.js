import { useState } from "react";
import css from "./CertificateForm.module.css";
import Input from "../Input.js"
import { useDispatch } from "react-redux";
import { setCertificate } from "../../../store/actions/certificateAction.js";
import { useRouter } from "next/router";

const formElements = [
  {
    name: "name",
    type: "text",
    title: "Name",
  },
  {
    name: "title",
    type: "text",
    title: "Title*",
    required: true,
  },
]

export default function CertificateForm({view}) {
  const [file, setFile] = useState("");

  const dispatch = useDispatch();
  const router = useRouter();

  const handleChange = (target) => {
    setFile(target.files[0]);
    const reader = new FileReader();
    reader.readAsDataURL(target.files[0]);
    reader.onload = (e) => {
      const newUrl = e.target.result;
      setFile(newUrl);
    };
  };

  const submit = (e) => {
    e.preventDefault();
    const {name, title, private_description, public_description} = e.target;
    const data = {
      name: name.value,
      title: title.value,
      private_description:private_description.value, 
      public_description: public_description.value,
      file
    }
    dispatch(setCertificate(data));
    router.push("/view-certificate");
  }
  const submitCreate = (e) => {
    e.preventDefault()
  }
  return <div>
    <form onSubmit={view ? submitCreate : submit} className={`${css.form} ${view ? "col-12" :"col-lg-6 col-11"}`}>
      {formElements.map((item) => <Input key={item.name} {...item} />)}
      <label className="input input-small profile-row">
        <span className="input__top d-flex align-items-center justify-content-between">
          <span className="bd-font input__title">Public Description</span>
        </span>
        <textarea name="public_description" className="input__field textarea"></textarea>
      </label>
      <label className="input input-small profile-row">
        <span className="input__top d-flex align-items-center justify-content-between">
          <span className="bd-font input__title">Private Description</span>
        </span>
        <textarea name="private_description" className="input__field textarea"></textarea>
      </label>
      <label className={css.upload}>
        <input type="file" onChange={({ target }) => handleChange(target)} name="file" hidden />
        <span>Upload File</span>
      </label>
      <button className={`${view ? "btn-solid-accent" : "btn-fill-accent"} btn btn-round`}>
        Preview
      </button>
    </form>
  </div>;
}
