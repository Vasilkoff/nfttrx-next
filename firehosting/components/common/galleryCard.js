import Link from "next/link";
import { Networks } from "../../constants/wallet.js";

export default function GalleryCard({
  id,
  image_url,
  name,
  author,
  likes,
  blockchain,
  price,
  priceCurrency,
  state,
  token_address,
  isMinted,
  ...props
}) {
  return (
    <Link
      href={
        state === "listing"
          ? `/gallery/${id}`
          : `/preview/${id || token_address + "?minted=" + !!isMinted}`
      }
    >
      <a className="square my-gallery-page-col-4 gallery-item">
        <div className="square__top">
          <div className="square__thumb">
            <img
              src={image_url || "/assets/img/example/gallery/1.jpg"}
              className="img-absolute"
            />
          </div>
          <div className="square-info d-flex align-items-center">
            <div className="square-info__number">{likes || 0}</div>
            <button
              onClick={(e) => e.preventDefault()}
              className="square-info__wishlist"
            >
              &nbsp;
            </button>
          </div>
        </div>
        <div className="square__content">
          <div className="square-top text-2">
            <p className="square-top__info link link-gray-second">
              {author || ""}
            </p>
            <div className="gray-second-color square-top__info">Min Bit</div>
          </div>
          <div className="square-more">
            <p className="square-more__item link link-title ">{name}</p>
            {state !== "draft" && (
              <div className="square-more__item">
                <div className="square-currency">
                  {priceCurrency === "GBP" ? (
                    <span className="mr-1">£</span>
                  ) : (
                    <img
                      src={
                        Networks[blockchain]?.image || Networks.ropsten?.image
                      }
                      className="square-currency__icon"
                    />
                  )}
                  <div className="square-currency__value">{price || "0"}</div>
                </div>
              </div>
            )}
          </div>
        </div>
      </a>
    </Link>
  );
}
