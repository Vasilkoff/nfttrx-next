import Link from "next/link";
import { useState } from "react";
import socialMedias from "../../../constants/socialMedias";
import css from "../../../styles/my-gallery.module.css";

function UserCard({avatar, userName, occupation, bio, socialMedia, isMyCard, props}) {
  const [activeInfo, setActiveInfo] = useState(true);
  const socialM = socialMedia || {};
  const socialMediasKey = Object.keys(socialM);

  return (
    <div className={css.aside}>
      {isMyCard && <Link href="/profile">
        <a className={"d-flex justify-content-end " + css.edit}>
          <img src="/assets/img/icons/edit.png" />
        </a>
      </Link>}
      <img src={avatar || "/assets/img/icons/avatars.svg"} />
      <div className={css.name}>
        {userName}
        <img src="/assets/img/icons/link.png" />
      </div>
      <div className={css.aside_text}>{occupation}</div>
      <div
        onClick={() => {
          setActiveInfo(!activeInfo);
        }}
        className={css.more_info}
      >
        {activeInfo ? "Less" : "More"} Info
      </div>
      <div className={css.biography + " " + (activeInfo ? css.activeInfo : "")}>
        <div className={css.aside_title}>Biography</div>
        <div>{bio}</div>
      </div>
      <div
        className={
          " col-12 " +
          css.social_media +
          " " +
          (activeInfo ? css.activeInfo : "")
        }
      >
        <div className={`d-flex justify-content-start ${css.aside_title}`}>
          Social media
        </div>
        <div className={css.medias}>
          {socialMediasKey.map((media) => {
            const [item] = socialMedias?.filter((el) => el.name === media);
            if (socialMedia[media]) {
              return (
                <a target="_blank" key={media} href={socialMedia[media]}>
                  <img src={item?.img} />
                </a>
              );
            }
          })}
        </div>
      </div>
    </div>
  );
}

export default UserCard;
