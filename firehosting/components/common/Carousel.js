import {useRef, useState, useEffect} from "react";

const Carousel = () => {
  const swiper = useRef(null);
  const [position, setPosition] = useState(0);
  
  const handleNext = () => {
    if(position < swiper.current.children?.length - 1) {
      setPosition(position + 1);
    }
  }
  
  const handlePrev = () => {
    if(position > 0) {
      setPosition(position - 1);
    }
  }

  useEffect(() => {
    swiper.current.style.left = "-" + (position * 100) + "%"
  }, [position])

  return (
    <div className="slider sale__slider">
      <div className="swiper-container sale__carousel js-sale-carousel">
        <div ref={swiper} className="swiper-wrapper">
          <div className="swiper-slide">
            <div className="card card-small sale-item">
              <img src="/assets/img/example/sale-item-1.jpg" className="img-absolute" />
              <div className="card-info d-flex flex-column">
                <div className="card-info__label">Fillip Started</div>
                <div className="card-info__title">Spectrum of a Ramenfication</div>
                <div className="card-info__more">1.05 ETH ($2,267.00)</div>
              </div>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="card card-small sale-item">
              <img src="/assets/img/example/sale-item-2.jpg" className="img-absolute" />
              <div className="card-info d-flex flex-column">
                <div className="card-info__label">Fillip Started</div>
                <div className="card-info__title">Spectrum of a Ramenfication</div>
                <div className="card-info__more">1.05 ETH ($2,267.00)</div>
              </div>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="card card-small sale-item">
              <img src="/assets/img/example/sale-item-3.jpg" className="img-absolute" />
              <div className="card-info d-flex flex-column">
                <div className="card-info__label">Fillip Started</div>
                <div className="card-info__title">Spectrum of a Ramenfication</div>
                <div className="card-info__more">1.05 ETH ($2,267.00)</div>
              </div>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="card card-small sale-item">
              <img src="/assets/img/example/sale-item-1.jpg" className="img-absolute" />
              <div className="card-info d-flex flex-column">
                <div className="card-info__label">Fillip Started</div>
                <div className="card-info__title">Spectrum of a Ramenfication</div>
                <div className="card-info__more">1.05 ETH ($2,267.00)</div>
              </div>
            </div>
          </div>
          <div className="swiper-slide">
            <div className="card card-small sale-item">
              <img src="/assets/img/example/sale-item-2.jpg" className="img-absolute" />
              <div className="card-info d-flex flex-column">
                <div className="card-info__label">Fillip Started</div>
                <div className="card-info__title">Spectrum of a Ramenfication</div>
                <div className="card-info__more">1.05 ETH ($2,267.00)</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <button onClick={handlePrev} className="d-none d-md-flex slider__btn slider__prev js-sale-carousel-prev">
        <svg width="28" height="28" viewBox="0 0 28 28" xmlns="http://www.w3.org/2000/svg"
          className="slider__icon">
          <path d="M17.5 21L10.5 14L17.5 7" />
        </svg>
      </button>
      <button onClick={handleNext} className="d-none d-md-flex slider__btn slider__next js-sale-carousel-next">
        <svg width="28" height="28" viewBox="0 0 28 28" xmlns="http://www.w3.org/2000/svg"
          className="slider__icon">
          <path d="M10.5 21L17.5 14L10.5 7" />
        </svg>
      </button>
    </div>
  )
}

export default Carousel;