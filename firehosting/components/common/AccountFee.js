import React, { useEffect, useState } from "react";
import { db } from "../../config/firebase.js";
import css from "../../styles/auth.module.css";
import { useSelector } from "react-redux";
import { getAuth } from "../../store/selectors.js";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import Api from "../../api/api.js";

const options = {
  style: {
    base: {
      color: "#32325d",
      fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
      fontSmoothing: "antialiased",
      fontSize: "16px",
      "::placeholder": {
        color: "#aab7c4",
      },
    },
    invalid: {
      color: "#fa755a",
      iconColor: "#fa755a",
    },
  },
};

const AccountFee = () => {
  const [clientSecret, setClientSecret] = useState(null);
  const [error, setError] = useState("");
  const [sending, setSending] = useState(false);
  const user = useSelector(getAuth);
  const stripe = useStripe();
  const elements = useElements();

  useEffect(() => {
    Api.createPaymentIntent({
      payment_method_types: ["card"],
    }).then((res) => {
      setClientSecret(res.data);
    });
  }, []);

  const cancel = () => {
    db.collection("Users")
      .doc(user?.uid)
      .update({ feeCanceled: true })
      .then(() => {
        window.location.reload();
      });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    setSending(true);

    if (elements == null) {
      setSending(false);
      return;
    }
    const payload = await stripe.confirmCardPayment(
      clientSecret.client_secret,
      {
        payment_method: {
          card: elements.getElement(CardElement),
          billing_details: {
            name: event.target.name.value,
          },
        },
      }
    );

    if (payload?.paymentIntent) {
      db.collection("Users")
        .doc(user?.uid)
        .update({ accountFee: true, feeDetails: payload?.paymentIntent })
        .then(() => {
          window.location.reload();
        });
    } else {
      setError(payload?.error?.message);
    }
    setSending(false);
  };

  return (
    <div className={css.wrapper + " col-12 col-md-6"}>
      <p className={css.welocome}>Step 3</p>
      <h3 className={css.header}>Open account fee</h3>
      <p className={css.description}>
        One-time payment to cover our operating expenses.
      </p>
      <form onSubmit={handleSubmit} className={css.cardFormWrapper}>
        <CardElement className="stripe-element" options={options} />
        <br />
        <div className="error-message">{error}</div>
        <div className={css.btnsWrapper}>
          <button onClick={cancel} type="button" className="btn">
            Skip
          </button>
          <button
            type="submit"
            className="btn btn-fill-accent main-btn-radius"
            disabled={!stripe || !elements || sending}
          >
            Pay 4 EUR
          </button>
        </div>
      </form>
    </div>
  );
};

export default AccountFee;
