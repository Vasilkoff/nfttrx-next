import { useState } from "react";
import { useSelector } from "react-redux";
import { db } from "../../../config/firebase";
import { getAuth } from "../../../store/selectors";
import css from "./DocumentStep.module.css";

function DocumentStep({ step, title, description, btnText, field, success }) {
  const [isSending, setSending] = useState(false);
  const { user, uid } = useSelector(getAuth);
  const handleClick = () => {
    setSending(true);
    db.collection("Users")
      .doc(uid)
      .update({ [field]: false })
      .finally(() => {
        window.location.reload();
      });
  };
  return (
    <div className={css.wrapper}>
      {step && <div className={css.step}> Step {step || 1}</div>}
      <div className={css.body}>
        <div>
          <div className={css.title}>{title}</div>
          <p className={css.description}>{description}</p>
        </div>
        <div className={css.checkBlock}>
          {user[field] || user[field] === undefined ? (
            <button
              disabled={isSending || user[field] === undefined }
              onClick={handleClick}
              className="btn btn-fill-accent main-btn-radius"
            >
              {btnText}
            </button>
          ) : (
            <div className={css.success}>
                {success}
                <img src="/assets/img/icons/success.png" alt="Done" />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default DocumentStep;
