import React, { useState } from "react";

export default function SocialMediaCard({
  value,
  isUpdated,
  changeMedia,
  name,
  img,
  globalVal,
  pattern,
  setSocialMedias,
  error,
}) {
  const [active, setActive] = useState(true);

  React.useEffect(() => {
    if (isUpdated) {
      setActive(true);
    }
  }, [isUpdated]);

  return (
    <div className="social-medias_container">
      <div className="social-medias d-flex align-items-center justify-content-between">
        <div className="d-flex">
          <img width={"32px"} style={{ marginRight: "10px" }} src={img} />
          <span>{name}</span>
        </div>
        <input
          value={value}
          onChange={(e) => {
            const value = e.target.value;
            if(!value) {
              setSocialMedias(name, true);
            } else if (!pattern.test(value)) {
              setSocialMedias(name);
            } else {
              setSocialMedias(name, true);
            }
            changeMedia(name, value);
          }}
          className={
            globalVal && active
              ? "active input__field social-medias_input"
              : "input__field social-medias_input"
          }
          type="url"
          maxLength={100}
        />
        <div className={globalVal && active ? "active" : ""}>
          <div>{value}</div>
          <img
            onClick={() => {
              setActive(false);
            }}
            width={"20px"}
            src="/assets/img/icons/x.png"
          />
        </div>
      </div>
      <div className="error-message">{error}</div>
    </div>
  );
}
