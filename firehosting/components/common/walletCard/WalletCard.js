import Link from "next/link";
import { useSelector } from "react-redux";
import { useState } from "react";
import css from "../../../styles/wallet.module.css";
import { Networks } from "../../../constants/wallet.js";

export default function WalletCard({ isImport }) {
  const wallet = useSelector((state) => state.wallet.selectedWallet);
  const selectedNetwork = useSelector((state) => state.wallet.selectedNetwork);

  const [isUpdated, setIsUpdated] = useState(false);

  const copyKey = (str) => {
    navigator.clipboard.writeText(str);
    updateSuccess()
  };
  const updateSuccess = () => {
    setIsUpdated(true);
    setTimeout(() => setIsUpdated(false), 1700);
  };
  if (isImport) {
    return (
      <div className={`${css.wallet} ${css.cardWallet}`}>
        <img
          className={"col-6 col-md-3 mt-5"}
          src="/assets/img/elements/white-logo.png"
          alt="NFTTRX"
        />
        <h1 className={`${css.welcome} mb-5`}>Welcome WALLET</h1>
      </div>
    );
  }
  const balance = wallet?.["balance" + selectedNetwork]
  const totalBalance = balance?.length > 8 ? Number(balance).toFixed(8) + "..." : balance;
  return (
    <>
      <div className={css.updated + " " + (isUpdated && css.show)}>
        Copied to clipboard
      </div>
      <div>
        <div className={css.wallet}>
          <img
            className={css.logo}
            src="/assets/img/elements/white-logo.png"
            alt="NFTTRX"
          />
          <div className="d-flex align-items-end justify-content-between flex-wrap">
            <div>
              <p className={css.total}>{wallet?.name}:</p>
              <h2 className={css.amount}>
                {
                  totalBalance || "0.0"
                }
                {
                  ` ${Networks[selectedNetwork].key}`
                }
                <img
                  onClick={() => copyKey(balance || "0.0")}
                  src="/assets/img/elements/copy.png"
                  alt="COPY"
                />
              </h2>
              <div className={css.walletNumber}>
                <a
                  target="_blank"
                  href={Networks[selectedNetwork].EtherscanAddress + wallet?.Address}
                >{wallet?.Address}</a>
                <img
                  onClick={() => copyKey(wallet?.Address)}
                  src="/assets/img/elements/copy.png"
                  alt="COPY"
                />
              </div>
            </div>
            <div className={css.actionsWrapper}>
              <Link href="/wallet/send">
                <a className={css.action}>
                  <img src="/assets/img/icons/arrow-up-circle.png" alt="SEND" />
                  <p>Send</p>
                </a>
              </Link>
              <Link href="/wallet/receive">
                <a className={css.action}>
                  <img
                    className={css.receive}
                    src="/assets/img/icons/arrow-up-circle.png"
                    alt="SEND"
                  />
                  <p>Receive</p>
                </a>
              </Link>
            </div>
          </div>
        </div>
        <div
          className={`${css.smallActionsWrapper} mt-4 justify-content-between`}
        >
          <Link href="/wallet/send">
            <a className={`${css.action} col-5`}>
              <img src="/assets/img/icons/arrow-up-circle.png" alt="SEND" />
              <p>Send</p>
            </a>
          </Link>
          <Link href="/wallet/receive">
            <a className={`${css.action} col-5`}>
              <img
                className={css.receive}
                src="/assets/img/icons/arrow-up-circle.png"
                alt="SEND"
              />
              <p>Receive</p>
            </a>
          </Link>
        </div>
      </div>
    </>
  )
}
