import React, { useRef } from "react";
import css from "../../styles/modal.module.css";
import { useOutsideClick } from "../../hooks/outsideClick.js";

const Modal = ({ Component, ...props }) => {
  const wrapper = useRef(null);
  useOutsideClick(wrapper, props.onClose);

  return (
    <div className={css.Modal__Overlay}>
      <div className={css.Modal__Content}>
        <div ref={wrapper} className={css.wrapper}>
          <Component {...props} />
        </div>
      </div>
    </div>
  )
}

export default Modal;