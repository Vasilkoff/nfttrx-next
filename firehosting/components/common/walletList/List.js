import Link from "next/link";
import css from "../../../styles/wallet.module.css";

export default function List(
  { isLast, isAsset, symbol, realBalance, name, amount, image_url, token_address, ...props }
) {

  return <Link href={`/wallet/transactions-list/${token_address}/${isAsset ? symbol : ""}`} >
    <a
      className={`d-flex justify-content-between align-items-center ${css.listItem} ${isLast ? css.last : ""}`}
    >
      <div className={`d-flex align-items-center`}>
        <img
          className={css.tokenLogo}
          src={image_url || "/assets/img/icons/empty-token.webp"}
          alt="ICON"
        />
        <div className="ml-3">
          <div className={css.listTitle}>{symbol || "USDT"}</div>
          <div className={css.listSubTitle}>{name}</div>
        </div>
      </div>
      <div className={css.listAmount}>
        <div className={css.listTitle}>{realBalance || amount}</div>
      </div>
    </a>
  </Link>;
}
