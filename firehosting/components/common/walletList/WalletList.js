import css from "../../../styles/wallet.module.css";
import { useRouter } from "next/router";
import List from "./List.js";
import { useSelector } from "react-redux";
import { useCallback } from "react";

const ASSET = "Tokens";
const COLLECTIBLES = "NFTs";

export default function WalletList() {
  const { query: { q }, push } = useRouter();
  const { tokensData, nftsData } = useSelector(state => state.wallet)

  const renderAssets = useCallback(() => tokensData?.map(
    (asset, index, arr) => <List
      {...asset}
      key={asset?.token_address}
      isLast={++index === arr.length}
      isAsset
    />

  ), [tokensData]);

  const renderCollectibles = useCallback(() => nftsData?.map(
    (item, index, arr) => <List
      {...item}
      key={item?.token_id}
      isLast={++index === arr.length}
    />
  ), [nftsData]);

  const renderEmptyList = (col) => {
    return <div className={css.listItem + " " + css.empty}>You have no {col || ASSET}</div>
  };

  const renderList = useCallback(() => {
    const res = q === COLLECTIBLES ? renderCollectibles() : renderAssets();
    if (res.length) {
      return res
    }
    return renderEmptyList(q)
  }, [tokensData, nftsData, q]);

  return <>
    <div className={`${css.tabs} d-flex justify-content-between align-items-center`}>
      <div className={`${css.nav} d-flex align-items-center`}>
        <div
          className={q === ASSET || q !== COLLECTIBLES ? css.active : ""}
          onClick={() => push("/wallet?q=" + ASSET)}
        >Tokens</div>
        <div
          className={q === COLLECTIBLES ? css.active : ""}
          onClick={() => push("/wallet?q=" + COLLECTIBLES)}
        >NFTs</div>
      </div>
    </div>
    {renderList()}
  </>;
}
