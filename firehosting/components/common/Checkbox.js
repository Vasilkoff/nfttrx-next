import React, { useState } from "react";
import css from "../../styles/checkbox.module.css"

export default function Checkbox({ title, ...props}) {
    const [check, setCheck] = useState(false);

    return (
        <>
            <label className={css.wrapper}>
                <input
                    {...props}
                    onChange={(e) => setCheck(e.target.checked)}
                    className={css.hidden} 
                    type="checkbox"
                />
                <div className={css.checkbox}>
                    {check && <img src="/assets/img/check.png" alt="check" />}
                </div>
                <p className={css.title + " agreement"}>{title}</p>
            </label>
        </>
    )
}
