import Router from "next/router";
import css from "../../styles/wallet.module.css";


export default function WalletHeader({ title, name }) {
    const onGoBack = () => {
        Router.back();
    }
    return <div className={css.walletHeader}>
        <span onClick={onGoBack} className="link">
            <img width={"25"} src="/assets/img/icons/arrow.png" alt="<" />
        </span>
        <div className={css.headerTitle}>
            {title}
            <b> {name}</b>
        </div>
    </div>;
}
