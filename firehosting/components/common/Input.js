import React from 'react'
import css from "../../styles/input.module.css"


export default function Input({ title, ...props }) {
    return (
        <label className={css.wrapper}>
            <p className={css.title}>{title}</p>
            <input
                className={css.input}
                {...props}
            />
        </label>
    )
}
