import { useEffect, useState } from "react";
import useMobile from "../../../hooks/useMobile";
import css from "./Steps.module.css";

const Step = ({ title, info, position, current, last }) => {
  const isCurrent = current === position ? css.active : "";
  const islastElem = last === position ? css.last : "";
  return (
    <div className={`${css.step}`}>
      <div className={`${css.number} ${isCurrent} ${islastElem}`}>{position}</div>
      <div>
        <div className={css.title}>{title}</div>
        <div className={css.info}>{info}</div>
      </div>
    </div>
  );
};

export default function Steps({ step, steps, setSteps, stepsConst }) {  
  const isMobile = useMobile(996);

  useEffect(() => {
    if(isMobile) {
      const newSteps = [...stepsConst];
      if(step === 4) {
        setSteps(newSteps.splice(2, 3))
      } else if(step === 1) {
        setSteps(newSteps.splice(step - 1, step + 1))
      } else {
        setSteps(newSteps.splice(step - 1, step))
      }
    } else {
      setSteps([...stepsConst]);
    }
  }, [isMobile])

  return (
    <div className={css.wrapper}>
      <div>
        {steps.map((item, index) => (
          <Step
            key={item.title}
            last={stepsConst.length}
            current={step}
            index={index + 1}
            {...item}
          />
        ))}
      </div>
    </div>
  );
}
