import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { db } from "../../config/firebase";
import Preloader from "../common/preloader/Preloader";
import UserCard from "../common/userCard/UserCard";
import css from "../../styles/my-gallery.module.css";
import { state } from "../../constants/item";
import GalleryCard from "../common/galleryCard";

const UserPage = () => {
  const router = useRouter();
  const { id } = router.query;

  const [user, setUser] = useState(null);
  const [userItems, setUserItems] = useState(null);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    db.collection("Users")
      .doc(id)
      .get()
      .then((res) => {
        if (res.exists) {
          setLoading(false);
          setUser(res.data());
        } else {
          router.push("/gallery/user");
        }
      })
      .catch(() => {
        router.push("/gallery/user");
      });

    db.collection("UserItems")
      .where("ownerUid", "==", id)
      .where("state", "==", state.listing)
      .get()
      .then((res) => {
        const data = res.docs.map((el) => ({ ...el.data(), id: el.id }));
        setUserItems(data);
      });
  }, [id]);

  if (isLoading) return <Preloader />;
  return (
    <section className="section page profile">
      <div className={"container-big " + css.container}>
        <div className="profile-row d-flex flex-wrap justify-content-center">
          <UserCard {...user} />
          <div className={"profile-content " + css.wrapper}>
            <div className={"gallery-row d-flex flex-wrap"}>
              {userItems?.map((item, index) => (
                <GalleryCard {...item} key={item?.id || index} />
              ))}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default UserPage;
