import { useEffect, useRef, useState } from "react";
import { db } from "../../../config/firebase";
import css from "./AgencyAgreement.module.css";
import TemplateComponent from "react-mustache-template-component";
import SignatureCanvas from "react-signature-canvas";
import Api from "../../../api/api";
import { getCurrentDateDMY } from "../../../utils/getCurrentDateDMY";
import { useSelector } from "react-redux";
import { getAuth } from "../../../store/selectors";

function AgencyAgreement() {
  const { user, uid } = useSelector(getAuth);
  const name =
    user?.firstName && user?.lastName
      ? user?.firstName + " " + user?.lastName
      : "";

  const [mustache, setMustache] = useState("");
  const [submiting, setSubmiting] = useState();
  const [template, setTemplate] = useState(null);
  const [signatureStarted, setSignatureStarted] = useState(false);
  let sigPad = useRef();
  const newDate = getCurrentDateDMY();

  const data = {
    ...template,
    StartDate: newDate,

    PrincipalName: name,
    PrincipalEmail: user?.email || "",
    PrincipalSignature: "",
    PrincipalAddress:
      "<input required placeholder='Address' class='agreement-template-input' name='PrincipalAddress' type='text'>",
  };
  useEffect(() => {
    db.collection("templates")
      .doc("AgentAgreement")
      .get()
      .then((res) => {
        if (res.exists) {
          const { mustache, AgentSignature, ...data } = res.data();
          setMustache(mustache);
          const signature = `<img src="${AgentSignature}" alt="signature" class="agreement-template-signature" />`;
          setTemplate({ ...data, AgentSignature: signature });
        }
      });
  }, []);

  const onSkipAgencyAgreement = () => {
    db.collection("Users")
      .doc(uid)
      .update({ isAgencyAgreementCanceled: true })
      .then(() => {
        window.location.reload();
      });
  };

  const clearSig = () => {
    sigPad.clear();
    setSignatureStarted(false);
  };

  const addPdfIdToUser = ({ pdfDocId }) => {
    db.collection("Users")
      .doc(uid)
      .update({ pdfDocId, isAgencyAgreement: true });
  };
  const addPdfLinkToUser = ({ pdfDocLink }) => {
    db.collection("Users")
      .doc(uid)
      .update({ pdfDocLink, isAgencyAgreement: true })
      .then(() => {
        window.location.reload();
      });
  };

  const onReady = (e) => {
    e.preventDefault();
    const address = e.target[0].value;

    setSubmiting(true);
    const img = sigPad.getTrimmedCanvas().toDataURL("image/png");
    const variables = { ...data };

    variables.PrincipalSignature = `<img src='${img}' alt="sgnature" class="agreement-template-signature" />`;
    variables.PrincipalEmail = user?.email || "";
    variables.PrincipalAddress = address;
    variables.StartDate = newDate;
    variables.PrincipalAddress = address;

    db.collection("docs")
      .add({
        html: "",
        template: mustache,
        vars: JSON.stringify(variables),
      })
      .then((res) => {
        addPdfIdToUser({ pdfDocId: res.id });
        Api.getPdfLink(res.id).then((res) => {
          addPdfLinkToUser({ pdfDocLink: res.data });
        });
      });
  };
  return (
    <form onSubmit={onReady} className={css.wrapper}>
      <h1 className={css.title}>Signing our agency agreement</h1>
      <div className={css.paper}>
        <TemplateComponent template={mustache} data={data} />
        <div className={css.canvasWrapper}>
          <SignatureCanvas
            onBegin={() => setSignatureStarted(true)}
            penColor="black"
            canvasProps={{
              className: css.sigCanvas,
            }}
            ref={(ref) => (sigPad = ref)}
          />
          <img
            onClick={clearSig}
            className={`${css.refreshBtn} ${
              signatureStarted ? css.refreshActive : ""
            }`}
            src="https://cdn-icons-png.flaticon.com/512/25/25429.png"
            alt="refresh"
          />
        </div>
      </div>
      <div className={css.canvasWrapper}>
        <div className="mt-4">
          <div
            onClick={onSkipAgencyAgreement}
            className="mr-2 btn  main-btn-radius"
          >
            Skip
          </div>
          <button
            disabled={submiting}
            className="ml-2 btn btn-fill-accent main-btn-radius"
            type="submit"
          >
            {submiting ? "...Submiting" : "Agree"}
          </button>
        </div>
      </div>
    </form>
  );
}

export default AgencyAgreement;
