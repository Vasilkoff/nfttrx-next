import Link from "next/link";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Networks } from "../../constants/wallet";
import { getAllItemsInitialize } from "../../store/actions/initialize";
import FilterBar from "../common/filterBar/FilterBar";

const Card = ({
  image_url,
  author,
  name,
  priceCurrency,
  blockchain,
  price,
  id,
  state,
  ...props
}) => {
  console.log(props);
  return (
    <div className="square page-col-4 gallery-item in-wishlist">
      <div className="square__top">
        <Link href={state === "listing" ? `/gallery/${id}` : ""}>
          <a className="square__thumb">
            <img
              src={image_url || "/assets/img/example/gallery/2.jpg"}
              className="img-absolute"
            />
          </a>
        </Link>
        {/* <div className="square-info d-flex align-items-center">
          <div className="square-info__number">4</div>
          <button className="square-info__wishlist">&nbsp;</button>
        </div> */}
      </div>
      <div className="square__content">
        <div className="square-top text-2">
          <a href="#" className="square-top__info link link-gray-second">
            {author}
          </a>
          <div className="gray-second-color square-top__info">Max Bit</div>
        </div>
        <div className="square-more">
          <a href="#" className="square-more__item link link-title ">
            {name}
          </a>
          <div className="square-more__item">
            <div className="square-currency">
              {priceCurrency === "GBP" ? (
                <span className="mr-1">£</span>
              ) : (
                <img
                  src={Networks[blockchain]?.image || Networks.ropsten?.image}
                  className="square-currency__icon"
                />
              )}
              <div className="square-currency__value">{price || "0"}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const GalleryPage = () => {
  const mintedNfts = useSelector((state) => state.items.allItems);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAllItemsInitialize());
  }, []);
  return (
    <div>
      <section className="section page gallery">
        <div className="container-big">
          <FilterBar />
          <div className="gallery-row page-row d-flex flex-wrap">
            {mintedNfts.map((item) => (
              <Card key={item.id} {...item} />
            ))}
          </div>
        </div>
      </section>
    </div>
  );
};

export default GalleryPage;
