import React, { Component } from "react";
import Carousel from "../common/Carousel.js";
import Link from 'next/link';


export default class HomePage extends Component {
  render() {
    return (
      <div>
        <section className="screen">
          <div className="container">
            <div className="row align-items-center">
              <div className="col-lg-6">
                <div className="screen__content">
                  <div className="screen__title">
                    <h1>Your reliable <span className="mark">NFT</span> trading agent </h1>
                  </div>
                  <div className="screen__descr">Start here your NFT expirience SAFE!</div>
                  <div className="screen__btns d-flex flex-wrap">
                    <Link href="/gallery">
                      <a className="btn btn-round btn-fill-accent screen__buy">Buy</a>
                    </Link>
                    <Link href="/login">
                      <a className="btn btn-round btn-solid-accent screen__sell">Sell</a>
                    </Link>
                  </div>
                  <div className="screen__more">Buy or sell digital rights using the Tron blockchain, protect the intellectual property rights of your digital assets. Join Early Access.
                  </div>
                </div>
              </div>
              <div className="col-lg-6">
                <div className="card screen__card">
                  <img src="/assets/img/example/screen-img.jpg" className="img-absolute" />
                  <div className="card-info d-flex flex-column align-items-center">
                    <div className="card-info__title">Spectrum of a Ramenfication</div>
                    <div className="card-info__more d-flex align-items-center justify-content-center flex-wrap">
                      <div className="card-info__subtitle">Current BIT</div>
                      <div className="card-info__main"><strong>1.05 ETH</strong> ($2,267.00)</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div id="why-nfttrx" />

        <section id="why-nfttrx" className="section why">
          <div className="container">
            <div className="section__title section__title--margin">
              <h2>Why NFTTRX</h2>
            </div>
            <div className="why__items d-flex flex-wrap">
              <div className="why-item">
                <img src="/assets/img/icons/icon-refund-2-line.svg" className="why-item__icon" />
                <div className="why-item__title">
                  <h4>It’s completely free</h4>
                </div>
                <div className="why-item__descr">Unlike other NFT marketplaces that use Ethereum with high gas fees,
                  NFTTRX uses Tron blockchain technology to list and trade your items for FREE!
                </div>
              </div>
              <div className="why-item">
                <img src="/assets/img/icons/icon-team-line.svg" className="why-item__icon" />
                <div className="why-item__title">
                  <h4>We do all the work</h4>
                </div>
                <div className="why-item__descr">By signing an agreement with NFTTRX, you're giving us permission to
                  do all the work for you — literally! We‘ll list your work for you. You just collect your money.
                </div>
              </div>
              <div className="why-item">
                <img src="/assets/img/icons/icon-shield-cross-line.svg" className="why-item__icon" />
                <div className="why-item__title">
                  <h4>We are engaged in the legal</h4>
                </div>
                <div className="why-item__descr">At NFTTRX, you will be assigned a legal expert who will work.
                  directly with your art to ensure that all legal requirements are met, allowing you to focus 
                  on your work, not legal issues!
                </div>
              </div>
            </div>
          </div>
        </section>

        <section id="start" className="section getting">
          <div className="container container-relative">
            <img src="/assets/img/elements/cloud-big.png" className="getting__cloud getting__cloud-1" />
            <img src="/assets/img/elements/cloud-small.png" className="getting__cloud getting__cloud-2" />
            <div className="section__title">
              <h2>Getting Started is Easy</h2>
            </div>
            <div className="row justify-content-center">
              <div className="col-md-auto">
                <div className="getting__content">
                  <div className="getting-item d-flex">
                    <div className="getting-item__number">1</div>
                    <div className="getting-item__title">Fill out our form</div>
                  </div>
                  <div className="getting-item d-flex">
                    <div className="getting-item__number">2</div>
                    <div className="getting-item__title">We will send you a contract</div>
                  </div>
                  <div className="getting-item d-flex">
                    <div className="getting-item__number">3</div>
                    <div className="getting-item__title">Your art goes on our marketplace</div>
                  </div>
                  <div className="getting-item d-flex">
                    <div className="getting-item__number">4</div>
                    <div className="getting-item__title">We will send you the money from each sale!</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="section__btn d-flex justify-content-center">
              <a href="#" className="btn btn-round btn-fill-accent getting__btn">Get Started</a>
            </div>
          </div>
        </section>

        <section id="sale" className="section sale">
          <div className="container">
            <div className="section__title section__title--margin">
              <h2>Items on <span className="accent-color">SALE</span></h2>
            </div>
            <Carousel />
          </div>
        </section>
        <div id="faq" />

        <section className="section faq">
          <div className="container">
            <div className="section__title">
              <h2>Here Are FAQs We Get A Lot</h2>
            </div>
            <div className="faq__row d-flex flex-wrap">
              <div className="faq-block">
                <div className="faq-block__title">
                  <h3>What does NFT stand for?</h3>
                </div>
                <div className="faq-block__descr content">
                  <p>Non-replaceable token. "Not fungible" more or less means that it is unique and cannot
                    be replaced by something else. For example, bitcoin is fungible: exchange one bitcoin for
                    another and you get the same thing. However, the unique trading card is non-fungible. 
                    If you exchange it for another card, you will have something completely different.</p>
                </div>
              </div>
              <div className="faq-block">
                <div className="faq-block__title">
                  <h3>What is NFT?</h3>
                </div>
                <div className="faq-block__descr content">
                  <p>An NFT is a digital asset that represents real-world objects like art, music, in-game items, and
                    videos. They are bought and sold online, frequently with cryptocurrency, and they are generally
                    encoded with the same underlying software as many cryptos.</p>
                </div>
              </div>
              <div className="faq-block">
                <div className="faq-block__title">
                  <h3>How is NFT different from cryptocurrency?</h3>
                </div>
                <div className="faq-block__descr content">
                  <p>NFTs are different from cryptocurrencies. The main difference is that each NFT is digitally
                    signed, making it impossible for NFTs to be exchanged or equal to each other 
                    (hence non-fungible). For example, one NBA Top Shot clip is not equal to EVERYDAYS simply
                    because they are both NFTs.</p>
                </div>
              </div>
              <div className="faq-block">
                <div className="faq-block__title">
                  <h3>How do NFTs work?</h3>
                </div>
                <div className="faq-block__descr content">
                  <p>NFTs exist on the blockchain, which is a distributed public ledger in which transactions
                    are recorded. An NFT can only have one owner at a time, and the unique NFT data makes it easy
                    to verify their ownership as well as transfer tokens between owners. Another unique feature is
                    the ability of owners / creators to store certain information inside the NFT, for example:
                    artists can sign their work by including their signature in the NFT metadata.</p>
                </div>
              </div>
              <div className="faq-block">
                <div className="faq-block__title">
                  <h3>How did we make it free?</h3>
                </div>
                <div className="faq-block__descr content">
                  <p>If you are already trading NFT, you may have done so with Ethereum and you may have
                    been aware of the high gas fees. We have solved this problem with the Tron blockchain,
                    which allows you to list and sell your work without any transaction costs or gas fees.</p>
                </div>
              </div>
              <div className="faq-block">
                <div className="faq-block__title">
                  <h3>How do we protect your intellectual property?</h3>
                </div>
                <div className="faq-block__descr content">
                  <p>By using NFT, you can own your original work. To ensure your ownership, each NFT contains
                    built-in authentication that serves as proof of ownership.</p>
                </div>
              </div>
              <div className="faq-block">
                <div className="faq-block__title">
                  <h3>What are the guarantees?</h3>
                </div>
                <div className="faq-block__descr content">
                  <p>We guarantee that if you are not completely satisfied with our services, you have the
                    right to terminate your listing agreement by giving us 30 days written notice.</p>
                  <p>We understand that you need a quick response and don't want to wait days to hear from
                    us. Our team is committed to responding to your requests as quickly and as fully as possible.</p>
                  <p>You can trust us - we will not share your email address with others. We will never discuss your
                    personal situation with anyone. We will always treat you with respect. And we will always value 
                    your privacy. Just ask our clients.</p>
                  <p>We are here to serve you - whether it is subscribing to our newsletter, viewing the latest homes
                    to hit the market, receiving correspondence via email or otherwise, you can rest assured that we are
                    offering you these services free of charge and without any obligations of any kind. </p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  };
}
