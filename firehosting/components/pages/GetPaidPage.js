import Aside from "../common/Aside";
import css from  "../../styles/getpaid.module.css";
import { useState } from "react";
export default function GetPaidPage() {
  const [modal, setModal] = useState(false);
  const OpenModal = () => {
    setModal(true);
  };
  const closeModal = () => {
    setModal(false);
  };
  
  return (
    <section className="section page profile">
      <div className="container-big">
        <div className="profile-row d-flex flex-wrap justify-content-center">
          <Aside />
          <div className="profile-content">
            <div className="profile-inner">
              <div className={css.Wrapper}>
                <div className={css.border}>
                  <div className={css.text}>Balance</div>
                </div>
                <div className={css.border2}>
                  <div className={css.text2}>Your balance is $0.00</div>
                  <div className="profile-bottom d-flex align-items-center justify-content-center justify-content-md-end">
                    <button
                      disabled={true}
                      className="btn btn-round btn-middle btn-solid-accent profile-bottom__save"
                    >
                      Get Paid Now
                    </button>
                  </div>
                </div>
              </div>

              <div className={css.Wrapper}>
                <div className={css.border3}>
                  <div className={css.text}>Payment details</div>
                  <div className="profile-bottom d-flex align-items-center justify-content-center justify-content-md-end">
                    <button
                      onClick={OpenModal}
                      className="btn btn-round btn-middle btn-solid-accent profile-bottom__save"
                    >
                      Add Method
                    </button>
                  </div>
                </div>
                <div className={css.border4}>
                  <div className={css.text3}>
                    You have not set up any payment methods yet
                  </div>
                  <div className={css.text4}>
                    Tell us how you want to receive your funds. It may take up
                    to 3 days to activate your payment method.
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div onClick={closeModal}
            className={css.modalWrapper + " " + (modal ? css.active : "")}
          ></div>
          <div className={css.modal + " " + (modal ? css.active2 : "")}>
            <div className={css.Wrapper}>
              <div className={css.border}>
                <div className={css.text}>Add a payment method</div>
                <a onClick={closeModal} href="#" className={css.x}>
                  <img src="/assets/img/icons/x.png" />
                </a>
              </div>
              <div className={css.cart}>
                <img
                  className={css.modalImg}
                  src="/assets/img/icons/paypal.png"
                />
                <div className={css.info}>
                  <div className={css.name}>PayPal</div>
                  <div className={css.des}>
                    · PayPal may charge
                    <span className={css.color}> additional fees</span> for
                    sending and
                    <br /> withdrawing funds.
                    <br />· Set Up will take you to PayPal
                  </div>
                </div>
                <div
                  className={
                    css.btn +
                    " profile-bottom d-flex align-items-center justify-content-center justify-content-md-end"
                  }
                >
                  <button
                    className=" btn btn-round btn-middle btn-solid-accent profile-bottom__save"
                  >
                    Set Up
                  </button>
                </div>
              </div>
              <div className={css.cart}>
                <img
                  className={css.modalImg}
                  src="/assets/img/icons/peyoneer.png"
                />
                <div className={css.info}>
                  <div className={css.name}>Peyoneer</div>
                  <div className={css.des}>
                    · PayPal may charge
                    <span className={css.color}> additional fees</span> for
                    sending and
                    <br /> withdrawing funds.
                    <br />· Set Up will take you to PayPal
                  </div>
                </div>
                <div
                  className={
                    css.btn +
                    " profile-bottom d-flex align-items-center justify-content-center justify-content-md-end"
                  }
                >
                  <button
                    className=" btn btn-round btn-middle btn-solid-accent profile-bottom__save"
                  >
                    Set Up
                  </button>
                </div>
              </div>
              <div className={css.cart}>
                <img
                  className={css.modalImg2}
                  src="/assets/img/icons/tron.png"
                />
                <div className={css.info}>
                  <div className={css.name}>Tron</div>
                  <div className={css.des}>
                    · PayPal may charge
                    <span className={css.color}> additional fees</span> for
                    sending and
                    <br /> withdrawing funds.
                    <br />· Set Up will take you to PayPal
                  </div>
                </div>
                <div
                  className={
                    css.btn +
                    " profile-bottom d-flex align-items-center justify-content-center justify-content-md-end"
                  }
                >
                  <button
                    className=" btn btn-round btn-middle btn-solid-accent profile-bottom__save"
                  >
                    Set Up
                  </button>
                </div>
              </div>
              <div className={css.cart}>
                <img
                  className={css.modalImg2}
                  src="/assets/img/icons/bankcard.png"
                />
                <div className={css.info}>
                  <div className={css.name}>Bank Card</div>
                  <div className={css.des}>
                    · PayPal may charge
                    <span className={css.color}> additional fees</span> for
                    sending and
                    <br /> withdrawing funds.
                    <br />· Set Up will take you to PayPal
                  </div>
                </div>
                <div
                  className={
                    css.btn +
                    " profile-bottom d-flex align-items-center justify-content-center justify-content-md-end"
                  }
                >
                  <button
                    className=" btn btn-round btn-middle btn-solid-accent profile-bottom__save"
                  >
                    Set Up
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
