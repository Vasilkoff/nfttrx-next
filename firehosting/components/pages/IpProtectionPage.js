import Link from "next/link";
import css from "../../styles/protection.module.css";
import Certificate from "../common/certificate/Certificate.jsx";
import Slider from "react-slick";
import CertificateForm from "../common/certificatForm/CertificateForm.js";
import { useEffect } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";





export default function IpProtectionPage() {
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    
    responsive: [
      {
          breakpoint: 992, 
          settings: {
              slidesToShow: 2,
            
          }
      },
      {
          breakpoint: 765, 
          settings: {
              slidesToShow: 1,
              arrows: false,
          }
      }
  ]
  };
  return (
    <div>
      <main className={`container ${css.section} ${css.main}`}>
        <div className="row align-items-center justify-content-center">
          <div className="col-lg-6 col-10">
            <div className="screen__content">
              <div className="screen__title">
                <h1>
                  Here's an easy and reliable way to get proof of authorship for
                  <span className="mark"> your </span> intellectual property.
                </h1>
              </div>
              <div className="screen__descr">Use NFT as your intellectual p roperty right evidence.</div>
              <div className="screen__btns d-flex">
                <a href="#" className='btn btn-round btn-fill-accent screen__buy'>Get started</a>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-md-10 col-11">
            <Certificate isSale />
            {/* <img className={css.mainCertificate} src="/assets/img/elements/certificate.jpg" /> */}
          </div>
        </div>
      </main>
      <section className={`section sale ${css.section}`}>
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <img src="/assets/img/elements/Phone-Mockup.png" alt="Phone" />
            </div>
            <div className="col-lg-1" />
            <div className={`col-lg-5 ${css.textBlog}`}>
              <div className="section__title">
                <h2>App Features</h2>
              </div>
              <p className={css.description}>
                Obtaining a patent is not an easy process. It’s not cheap either.
                <br />
                In addition not everything can be a patent. Technical solutions are considered to be inventions. So, for example, a method of making a medicine can be patented, but it is impossible to protect the way of doing business.
                <br />
                <br />
                Let's say you come up with an idea for a business and expect to use it to enter a new market and make a good profit. However, from the moment when the idea occurred to you, it will take some time until it is implemented. During this time, competitors, investors, employees and even their own relatives can steal it. And now they have an innovative company and high incomes, and you are left with nothing.
              </p>
              <Link href="#">
                <a className={css.getStart}>
                  Get started
                  <img src="/assets/img/elements/right-arrow.png" alt="Get Start" />
                </a>
              </Link>
            </div>
          </div>
        </div>
      </section>
      <section className={`section ${css.section}`}>
        <div className="container">
          <div className="row">
            <div className={`col-lg-5 ${css.textBlog}`}>
              <div className="section__title">
                <h2>When copyright protection begins</h2>
              </div>
              <p className={css.description}>
                Copyright is a legal term used to describe the rights that authors have in their literary and artistic works. Copyright covers a range of works, from books, music, paintings, sculpture and films to computer programs, databases, advertisements, maps and technical drawings.
                Copyright applies to sketches, sketches, descriptions, artwork, databases, titles, but not to what comes to your mind. It is only the expression, and not the idea, that is protected by copyright law.
                <br />
                Until the author expresses them in some material form - does not draw or describe copyright protection does not begin.
                <br />
                <br />
                Until the author expresses them in some material form - does not draw or describe copyright protection does not begin.

              </p>
              <Link href="#">
                <a className={css.getStart}>
                  Get started
                  <img src="/assets/img/elements/right-arrow.png" alt="Get Start" />
                </a>
              </Link>
            </div>
            <div className="col-lg-1" />
            <div className="col-lg-6">
              <img src="/assets/img/elements/Phone Mockup.png" alt="Phone" />
            </div>
          </div>
        </div>
      </section>
      <section className={`section sale`} style={{ paddingBottom: "64px" }}>
        <div className={`container`}>
          <div className={`${css.solution}`}>
            <div className="section__title">
              <h2>We have a solution</h2>
            </div>
            <p className={`${css.description} col-lg-7 col-10`}>
              NFT gives you the opportunity to prove at any time and in any situation that your idea belongs to you and you had it at a certain point in time.
            </p>
          </div>
        </div>
      </section>

      <section className={`section ${css.section}`}>
        <div className="section__title">
          <h2>Registration</h2>
        </div>
        <CertificateForm />
      </section>

      <section className={`section sale`} style={{ paddingBottom: "64px" }}>
        <div className={`container`}>
          <div className="section__title">
            <h2>Ideas for <span className="accent-color">SALE</span></h2>
          </div></div>
          
          <div className={css.wrapper}>
          <Slider {...settings} 
             className={css.certificateWrapper + ' container' }
           >
            {
              [1, 2, 3, 4].map((item) => 
              <div key={item} className="  col-11">
                <Certificate name="Aman Asylbekov" isSmall />
                <div className={`card-info ${css.cardInfo}`}>
                  <div className="card-info__subtitle text-left">Current BIT</div>
                  <div className="card-info__title fz-16">Spectrum of a Ramenfication</div>
                  <div className="card-info__more d-flex align-items-center justify-content-left flex-wrap">
                    <div className="card-info__main fz-16"><strong>1.05 ETH</strong> ($2,267.00)</div>
                  </div>
                </div>
              </div>
              
              )
            }
            
          </Slider>
          </div>

      </section>
    </div>
  )
}
