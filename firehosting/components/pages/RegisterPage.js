import Register from "../common/Register.js";
import Steps from "../common/steps/Steps.jsx";
import { useSelector, useDispatch } from "react-redux";
import { setRegisterStep } from "../../store/actions/authAction.js";
import VerifyAccaunt from "../common/VerifyAccaunt.js";
import AccountFee from "../common/AccountFee.js";
import { useEffect, useState } from "react";
import { useRouter } from "next/router.js";
import fire from "../../config/firebase.js";
import { getAuth } from "../../store/selectors.js";
import VerificationKycBlockpass from "../common/verification-kyc/VerificationKycBlockpass.jsx";

const stepsConst = [
  {
    position: 1,
    title: "Account registration",
    info: "Provide us with you authentication options.",
  },
  {
    position: 2,
    title: "Email confirmation",
    info: "You will receive a letter in the mail, follow the link.",
  },
  {
    position: 3,
    title: "Identity verification",
    info: "Verify your identity to protect your account.",
  },
  {
    position: 4,
    title: "NFT Agency agreement",
    info: "You need to Sign an agreenent with Us",
  },
];

export default function RegsiterPage() {
  const step = useSelector((state) => state.auth.register.step);
  const [steps, setSteps] = useState(stepsConst);
  const data = useSelector(getAuth);
  const dispatch = useDispatch();
  const { query } = useRouter();

  useEffect(() => {
    if (data && step && query?.code && !data?.emailVerified) {
      fire
        ?.auth()
        .applyActionCode(query.code)
        .then(() => {
          dispatch(setRegisterStep(3));
        })
        .catch((error) => {
          console.log(error)
        })
    }
  }, []);

  const getStep = () => {
    switch (step) {
      case 1:
        return <Register />;
      case 2:
        // TODO: https://github.com/firebase/snippets-web/blob/5879ad7dedb29c6703bf94f760f6c55073c7c4ac/auth/custom-email-handler.js
        return <VerifyAccaunt />;
      case 3:
        return <VerificationKycBlockpass />;
      // case 4:
      //   return <VerificationKycBlockpass />;
      default:
        dispatch(setRegisterStep(1));
        return <Register />;
    }
  };

  return (
    <div className="row auth-page">
      <div className="col-lg-6 col-12">
        {/* <img className='auth-image' src='/assets/img/elements/auth-papper.jpg' /> */}
        <Steps step={step} steps={steps} setSteps={setSteps} stepsConst={stepsConst} />
      </div>
      {getStep()}
    </div>
  );
}
