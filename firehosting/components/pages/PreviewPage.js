import Select from "react-select";
import { Networks } from "../../constants/wallet.js";
import { CustomLabel } from "../common/walletHeader/WalletHeader.js";
import { useEffect, useMemo, useState } from "react";
import Link from "next/link";
import { db } from "../../config/firebase.js";
import { useRouter } from "next/router";
import Preloader from "../common/preloader/Preloader";
import { state } from "../../constants/item.js";
import { useSelector } from "react-redux";
import { getAuth, getUserMintedItems } from "../../store/selectors.js";

const pounds = "GBP";

export default function PreviewPage() {
  const [isLoading, setIsLoading] = useState(true);
  const [blockchain, setBlockchain] = useState("ropsten");
  const [priceCurrency, setPriceCurrency] = useState(pounds);
  const [price, setPrice] = useState("");
  const [data, setData] = useState(null);
  const [sending, setSending] = useState(false);

  const mintedNfts = useSelector(getUserMintedItems);
  const {uid, user} = useSelector(getAuth);

  const rout = useRouter();

  useEffect(() => {
    setPriceCurrency("");
  }, [blockchain]);

  const successResponce = (data) => {
    setIsLoading(false);
    setData(data);
    setBlockchain(data?.blockchain);
    setPrice(data?.price || "");
    setPriceCurrency(data?.priceCurrency);
  };

  const errorResponce = () => {
    rout.back();
  };

  useEffect(() => {
    if (rout.query?.minted) {
      const nft = mintedNfts.find((el) => el.token_address === rout.query.id);
      if (nft) {
        successResponce(nft);
      } else {
        errorResponce();
      }
    } else {
      db.collection("UserItems")
        .doc(rout.query.id)
        .get()
        .then((res) => {
          if (res.exists) {
            const data = res.data();
            successResponce(data);
          } else {
            errorResponce(m);
          }
        })
        .catch(() => {
          errorResponce();
        });
    }
  }, []);

  const { keys, ...net } = Networks;
  const networkKeys = Object.keys(net);
  const options = networkKeys?.map((network) => ({
    value: network,
    label: <CustomLabel network={network} Networks={Networks} />,
  }));

  const priceArr = [pounds, Networks[blockchain || "ropsten"]?.key];

  const priceOptions = useMemo(
    () =>
      priceArr?.map((currency) => ({
        value: currency,
        label: <CustomLabel network={currency} />,
      })),
    [blockchain]
  );

  const submit = (e) => {
    e.preventDefault();
    setSending(true);
    if (rout.query?.minted) {
      const { isMinted, ...nft } = data;
      nft.priceCurrency = priceCurrency || pounds;
      nft.price = price;
      nft.state = state.listing;

      db.collection("UserItems")
        .add(nft)
        .then(() => {
          rout.push("/my-gallery");
          setSending(false);
        });
    } else {
      const data = {
        blockchain: blockchain || "ropsten",
        priceCurrency: priceCurrency || pounds,
        price,
        state: state.listing,
      };
      // TODO: publish to blockchain
      db.collection("UserItems")
        .doc(rout.query.id)
        .update(data)
        .then(() => {
          db.collection("Users").doc(uid).update({itemsCount: (+user?.itemsCount || 0) + 1})
          rout.push("/my-gallery");
        });
    }
  };
  const selectedPrice = priceOptions.filter(
    (option) => option.value === priceCurrency
  );
  if (isLoading || !data) {
    return <Preloader />;
  }
  return (
    <div className="section review-wrapper">
      <form onSubmit={submit} className="review_left-block">
        <div className="container-big review-container">
          <div className="review-title">
            List my NFT for sale
            <div>
              This way you publish this item for sale on NFTTRX marketplace
            </div>
          </div>
          <div className="review_title">Blockchain</div>
          <Select
            onChange={(data) => {
              setBlockchain(data.value);
            }}
            options={options}
            defaultValue={options[0]}
            className={`review-select`}
          />
          <div className="review_title">Price</div>
          <div className="review_take-price">
            <Select
              value={selectedPrice.length ? selectedPrice : priceOptions[0]}
              onChange={(data) => {
                setPriceCurrency(data.value);
              }}
              options={priceOptions}
              defaultValue={priceOptions[0]}
              className={`col-12 col-sm-5 review-select`}
            />
            <input
              placeholder="Price"
              value={price}
              onChange={(data) => {
                setPrice(data.target.value);
              }}
              required
              type={"number"}
              style={{ height: "42px", padding: "0px 15px", margin: "5px 0" }}
              className="col-12 col-sm-6 input__field"
            />
          </div>
          <div className="review_des">
            It's simple. The download will start after you sign the contract.
            Designed to save you the headaches, our license covers some of the
            most popular uses for this image.
            <span> Sign an agreement</span>
          </div>
          <label className="review_checkbox">
            <input required type={"checkbox"} />
            <span>I agree to Terms of Use</span>
          </label>
          <div className="review_buttons">
            <div />
            {/* <Link href={"/create-new-item"}><button>Back to edit</button></Link> */}
            <div>
              <button className="btn review-btn">
                <Link href={"/my-gallery"}>
                  <a>Save for later</a>
                </Link>
              </button>
              <button
                disabled={sending}
                type="submit"
                className="btn btn-round btn-solid-accent review-btn"
              >
                {sending ? "Publishing..." : "Publish"}
              </button>
            </div>
          </div>
        </div>
      </form>
      <div className="review_right-block">
        <div className="container-big review-container">
          <div className="review-title">Preview</div>
          <div className="review_img_wrapper">
            <img
              src={
                data?.image_url || "/assets/img/example/gallery/2fullSize.jpg"
              }
            />
          </div>
          <div className="d-flex justify-content-between">
            <div>{data?.author}</div>
            <div>Max Bit</div>
          </div>
          <div className="d-flex justify-content-between">
            <div>{data?.name}</div>
            <div className="d-flex align-items-center">
              {priceCurrency === pounds || !priceCurrency ? (
                <span className="mr-2">£</span>
              ) : (
                <img
                  className="mr-2"
                  width={"21px"}
                  src={Networks[blockchain || "ropsten"]?.image}
                />
              )}
              <div>{price || 0}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
