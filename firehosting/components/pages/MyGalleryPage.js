import { useEffect } from "react";
import Link from "next/link";
import css from "../../styles/my-gallery.module.css";
import GalleryCard from "../common/galleryCard";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { getUserItemsInitialize } from "../../store/actions/initialize";
import { useRouter } from "next/router";
import { getUserMintedItems } from "../../store/selectors";
import UserCard from "../common/userCard/UserCard";

const navbarVars = [
  { name: "My NFTs", value: "" },
  { name: "Collected", value: "minted" },
  { name: "Drafts", value: "draft" },
  { name: "Listings", value: "listing" },
];

export default function MyGalleryPage() {
  const userData = useSelector((state) => state.auth.data);
  const user = userData?.user;
  const userItems = useSelector((state) => state.items.userItems);
  const userMintedItems = useSelector(getUserMintedItems);

  const { query } = useRouter();
  const { state = "" } = query;

  const dispatch = useDispatch();

  useEffect(() => {
    window.innerWidth < 960 ? setActiveInfo(false) : "";
  }, []);

  useEffect(() => {
    dispatch(
      getUserItemsInitialize(userData?.uid, state === "minted" ? "" : state)
    );
  }, [state]);

  const userMintedItemsFiltered = userMintedItems.filter((item) => {
    const nft = userItems.find((el) => el.token_address === item.token_address);
    if (!nft) {
      return item;
    }
    return null;
  });
  const items = !state
    ? [...userMintedItemsFiltered, ...userItems]
    : state === "minted"
    ? [
        ...userMintedItemsFiltered,
        ...userItems.filter((el) => el.token_address),
      ]
    : userItems;
  return (
    <section className="section page profile">
      <div className={"container-big " + css.container}>
        <div className="profile-row d-flex flex-wrap justify-content-center">
          <UserCard isMyCard={true} {...user} />
          <div className={"profile-content " + css.wrapper}>
            <div className={"profile-inner " + css.under_wrapper}>
              <div className="d-flex col-12 justify-content-between align-items-center flex-wrap">
                <div className="col-xl-7 col-lg-12 col-md-12">
                  {navbarVars.map((el) => {
                    return (
                      <Link key={el.value} href={"?state=" + el.value}>
                        <a
                          className={
                            "col-4 col-md-1 " +
                            (el.value === state && css.active)
                          }
                        >
                          {el.name}
                        </a>
                      </Link>
                    );
                  })}
                </div>
                <Link href={"/create-new-item"}>
                  <a className={css.createNewItem}>Create new item</a>
                </Link>
              </div>
            </div>
            <div className={"gallery-row d-flex flex-wrap"}>
              {items.map((item, index) => (
                <GalleryCard {...item} key={item?.id || index} />
              ))}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
