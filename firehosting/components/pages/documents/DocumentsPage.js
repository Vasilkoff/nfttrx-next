import { useSelector } from "react-redux";
import { getAuth } from "../../../store/selectors.js";
import Aside from "../../common/Aside.js";
import DocumentPdf from "../../common/documentPdf/DocumentPdf.jsx";
import DocumentStep from "../../common/documentStep/DocumentStep.jsx";
import { canceledStepsConst, feeCanceledStep } from "./constants.js";
import css from "./Documents.module.css";

export default function DocumentsPage() {
  const { user } = useSelector(getAuth);
  const { feeCanceled } = user;

  const renderDocsStep = () => {
    if (feeCanceled) {
      // return <DocumentStep {...feeCanceledStep} />;
      return null
    } else {
      return canceledStepsConst.map((item) => (
        <DocumentStep key={item.step} {...item} />
      ));
    }
  };

  const renderDocs = () => {
    if (user?.pdfDocLink) {
      return (
        <div className={css.documents}>
          <div className={css.myDocuments}>My documents</div>
          {user?.pdfDocLink && (
            <DocumentPdf text="Agency Agreement" link={user?.pdfDocLink} />
          )}
          {
            // TODO: write maping of (ip protection) pdfs with "DocumentPdf" component
          }
        </div>
      );
    }
  };
  return (
    <section className="section page profile">
      <div className="container-big">
        <div className="profile-row d-flex flex-wrap">
          <Aside />
          <div className={"profile-content"}>
            <div>{renderDocsStep()}</div>

            {renderDocs()}
          </div>
        </div>
      </div>
    </section>
  );
}
