export const canceledStepsConst = [
  {
    title: "Verify Identity",
    description: `To be ready to transfer Intellectual property in your name and be able to use our services it’s important to make identity verification`,
    btnText: "Get Verified",
    field: "kycVerificationCanceled",
    success: "Your identity verified",
    step: 1,
  },
  {
    title: "Signing our agency agreement",
    description: `Having an agent means you have another person on your team. And having a team is awesome`,
    btnText: "Sign the agreement",
    field: "isAgencyAgreementCanceled",
    success: "Agency agreement",
    step: 2,
  },
];

export const feeCanceledStep = {
  title: "Please, pay your account opening fee",
  description: `After that, you will be able to  verify your identity with us and sign our agency agreement.
  
      Completing these you will be able to have access to our company’s services.`,
  btnText: "Pay",
  field: "feeCanceled",
};
