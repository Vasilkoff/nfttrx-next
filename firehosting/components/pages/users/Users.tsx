import React from "react";
import Preloader from "../../common/preloader/Preloader";
import css from "./Users.module.css";
import { useCollection } from "../../../hooks/useCollection";

const UsersListHeader: React.FC = () => {
  return (
    <div className={css.itemWrapper}>
      <div>Collection</div>
      {/* <div>Floor Price</div> */}
      <div>Items</div>
    </div>
  );
};

interface UserType {
  id: string;
  firstName: string;
  lastName: string;
  avatar: string;
  itemsCount: string;
  floorPrice:  string;
}

const UserItem: React.FC<UserType & { index: number }> = ({
  index,
  firstName,
  lastName,
  avatar,
  itemsCount,
  // floorPrice,
}) => {
  const name = (firstName && lastName) ? `${firstName} ${lastName}` :  "No name";
  const items = itemsCount ? itemsCount : 0;
  return (
    <div className={`${css.itemWrapper} ${css.userItem}`}>
      <div className={css.author}>
        <span>{index + 1}</span>
        <div>
          <img src={avatar || "/assets/img/icons/avatars.svg"} alt={name} />
        </div>
        <span>{name}</span>
      </div>

      {/* <div>{floorPrice || "0"}</div> */}
      <div>{items}</div>
    </div>
  );
};

const UsersPage: React.FC = () => {
  const [isLoading, users] = useCollection({ collection: "Users" }, []);

  if (isLoading) {
    return <Preloader />;
  }
  return (
    <section className="section page profile">
      <div className={"container-big " + css.wrapper}>
        <h1 className={css.title}>NFT Users</h1>
        <p className={css.description}>
          The top NFTs on NFTTRX, ranked by volume, floor price and other
          statistics.
        </p>
        <div className={css.listWrapper}>
          <UsersListHeader />
          {users?.map((user, index) => (
            <UserItem key={user.id} index={index} {...user} />
          ))}
        </div>
      </div>
    </section>
  );
};

export default UsersPage;
