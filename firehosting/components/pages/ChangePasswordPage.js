import React from "react";
import Aside from "../common/Aside";
import css from "../../styles/change-password.module.css";
export default function ChangePasswordPage() {
  return (
    <section className="section page profile">
      <div className="container-big">
        <div className="profile-row d-flex flex-wrap">
          <Aside />
          <div className="profile-content">
            <form className="profile-inner">
              <label className="input input-small profile-row">
                <span className="input__top">
                  <span className="bd-font input__title">Old Password</span>
                </span>
                <input type="text" className="input__field" />
              </label>
              <label className="input input-small profile-row">
                <span className="input__top">
                  <span className="bd-font input__title">New Password</span>
                </span>
                <input type="text" className="input__field" />
                <p className={css.underInput}>Minimum 6 characters</p>
              </label>
              <label className="input input-small profile-row">
                <span className="input__top">
                  <span className="bd-font input__title">New Password</span>
                </span>
                <input type="text" className="input__field" />
              </label>
              <div className="profile-bottom d-flex align-items-center justify-content-center justify-content-md-end">
                  <button  className="btn btn-round btn-middle btn-solid-accent profile-bottom__save">Save Changes</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
}
