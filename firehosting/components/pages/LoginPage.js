import React, {useState} from "react";
import Login from "../common/Login.js";
import Steps from "../common/steps/Steps.jsx";

const stepsConst = [
  {
    position: 1,
    title: "Protect your Intellectual property with NFT. ",
    info: "Create NFT for your idea, art or any other digital item.",
  },
  {
    position: 2,
    title: "Buy, Sell or Exchange NFTs",
    info: "Our agent service guarantee your protection and level of security you deserve ",
  },
  {
    position: 3,
    title: "Enjoy easy to use crypto wallet.",
    info: "First and  easy to use wallet crypto wallet",
  },
  {
    position: 4,
    title: "NFT deals with fiat money",
    info: "Sell and buy for EURs and GBPs",
  },
  {
    position: 5,
    title: "NFT can contain additional secret information",
    info: "his kind of information is available only for the current NFT owners. Additionally to the technical restriction we care about protection of this Information by the law.",
  },
];

export default function LoginPage() {
  const step = 1;
  const [steps, setSteps] = useState(stepsConst);

  return (
    <div className="row auth-page">
      <div className="col-md-6 col-12">
        <Steps
          step={step}
          steps={steps}
          setSteps={setSteps}
          stepsConst={stepsConst}
        />
      </div>
      <Login />
    </div>
  );
}
