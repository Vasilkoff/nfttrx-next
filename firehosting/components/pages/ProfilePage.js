import { useState } from "react";
import { db, storageRef } from "../../config/firebase.js";
import useInput from "../../hooks/useInput.js";
import { getAuth } from "../../store/selectors.js";
import Aside from "../common/Aside.js";
import { useDispatch, useSelector } from "react-redux";
import { setNewUser, setUserAvatar } from "../../store/actions/authAction.js";
import css from "../../styles/profile.module.css";
import SocialMediaCard from "../common/SocialMediaCard.js";
import socialMediasConts from "../../constants/socialMedias.js";

export default function ProfilePage() {
  const { user, uid } = useSelector(getAuth);
  const display_name = useInput(user?.displayName || "");
  // TODO: fix userName
  const name = user?.firstName && user?.lastName ? user?.firstName + " " + user?.lastName : "";
  const isNameDiabled = !!name; 
  const user_name = useInput(name || user?.userName || "");
  const occupation = useInput(user?.occupation || "");
  const bio = useInput(user?.bio || "");
  const [avatar, setAvatar] = useState(user?.avatar || "");
  const [file, setFile] = useState(null);
  const [isChanged, setIsChanged] = useState(true);
  const [isUpdated, setIsUpdated] = useState(false);
  const [medias, setMedias] = useState(user?.socialMedia || {})
  const [socialMedias, setSocialMedias] = useState(socialMediasConts);

  const dispatch = useDispatch()

  const handleChange = (target) => {
    setFile(target.files[0]);
    const reader = new FileReader();
    reader.readAsDataURL(target.files[0]);
    reader.onload = (e) => {
      const newUrl = e.target.result;
      setAvatar(newUrl);
    };
  };

  const handleDelete = () => {
    setFile(null);
    if (file) {
      setAvatar(user?.avatar)
    } else {
      setIsChanged(false)
      setAvatar("")
    }
  }

  const changeMedia = (name, value) => {
    setMedias({ ...medias, [name]: value })
  }

  const getUrl = async (name) => await storageRef
    .ref()
    .child(name)
    .getDownloadURL()
    .then((url) => {
      return url
    })

  const updateSuccess = () => {
    setIsUpdated(true)
    setTimeout(() => setIsUpdated(false), 5000)
  }
  const submit = (e) => {
    e.preventDefault();
    
    const isErrorLink = socialMedias.find((el) => !!el.error)
    if(isErrorLink) {
      return 
    }

    setIsChanged(true)
    if (file) {
      storageRef.ref(file.name).put(file).then(() => {
        getUrl(file.name).then((url) => {
          dispatch(setUserAvatar(url))
          db.collection("Users").doc(uid).set({
            avatar: url,
          }, { merge: true })
            .then(updateSuccess)
        })
      });
    } else {
      dispatch(setUserAvatar(avatar))
      db.collection("Users").doc(uid).set({
        avatar: avatar,
      }, { merge: true })
    }

    const data = {
      displayName: display_name.value,
      userName: user_name.value,
      occupation: occupation.value,
      bio: bio.value,
      socialMedia: medias
    };
    db.collection("Users").doc(uid).set(data, { merge: true })
      .then(updateSuccess)
    dispatch(setNewUser(data))
    setFile(null);
  }

  const setSocialMediasError = (name, isClever) => {
    const newArr = socialMedias.map((el) => {
      if(isClever) {
        return {...el, error: ""}
      }
      if(el.name === name) {
        return {...el, error: "Provided link is not correct!"}
      }
      return el
    })
    setSocialMedias(newArr)
  }

  return (
    <>
      <div className={css.updated + " " + (isUpdated && css.show)}>Profile updated successfully</div>
      <section className="section page profile">
        <div className="container-big">
          <div className="profile-row d-flex flex-wrap">
            <Aside />
            <div className="profile-content">
              <form onSubmit={submit} onChange={() => setIsChanged(false)} className="profile-inner">
                <div className="file profile-top d-flex flex-wrap align-items-center js-file">
                  <div className="profile-thumb file__thumb">
                    <img src={avatar || "/assets/img/icons/avatars.svg"} alt="alt" className="img-absolute" />
                  </div>
                  <div className="file__btns d-flex justify-content-center align-items-center">
                    <label className="file__custom">
                      <input type="file" className="file__old" hidden onChange={({ target }) => handleChange(target)} />
                      <span className="btn btn-small btn-solid-accent btn-round file__add">Upload new picture</span>
                    </label>
                    <span onClick={handleDelete} className="btn btn-small btn-little btn-fill-gray btn-round file__delete">Delete</span>
                  </div>
                </div>
                <label className="input input-small profile-row">
                  <span className="input__top">
                    <span className="bd-font input__title">Display Name*</span>
                  </span>
                  <input type="text" name="display_name" className="input__field" required {...display_name} />
                </label>
                <label className="input input-small profile-row">
                  <span className="input__top">
                    <span className="bd-font input__title">Full Name</span>
                  </span>
                  <input type="text" name="user_name" disabled={isNameDiabled} className="input__field" {...user_name} />
                </label>
                <label className="input input-small profile-row">
                  <span className="input__top">
                    <span className="bd-font input__title">Occupation</span>
                  </span>
                  <input type="text" name="user_occupation" {...occupation} className="input__field" />
                </label>
                <label className="input input-small profile-row">
                  <span className="input__top d-flex align-items-center justify-content-between">
                    <span className="bd-font input__title">Bio</span>
                    <span className="input__info">{bio?.value?.length}/400</span>
                  </span>
                  <textarea {...bio} maxLength={400} name="user_bio" className="input__field textarea"></textarea>
                </label>
                <div className="social-medias_wrapper">
                  <div className="social-medias_title">Social media</div>
                  {
                    socialMedias?.map((el) =>
                      <SocialMediaCard
                        key={el.name}
                        isUpdated={isUpdated}
                        img={el.img}
                        value={medias[el.name]}
                        globalVal={user?.socialMedia ? user?.socialMedia[el.name] : ""}
                        name={el.name}
                        changeMedia={changeMedia}
                        pattern={el.pattern}
                        setSocialMedias={setSocialMediasError}
                        error={el?.error}
                      />

                    )
                  }

                </div>
                <div className="profile-bottom d-flex align-items-center justify-content-center justify-content-md-end">
                  <button disabled={isChanged} className="btn btn-round btn-middle btn-solid-accent profile-bottom__save">Save Changes</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}
