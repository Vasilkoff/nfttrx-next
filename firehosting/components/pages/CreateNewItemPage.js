import css from "../../styles/create-new-item.module.css";
import { useState } from "react";
import useInput from "../../hooks/useInput";
import Link from "next/link";
import { db, storageRef } from "../../config/firebase.js";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { state } from "../../constants/item.js";

export default function CreateNewItemPage() {
  const {uid, user} = useSelector((state) => state.auth.data);

  const [sending, setSending] = useState(false)
  const [success, setSuccess] = useState(false)

  const name = useInput("");
  const link = useInput("");
  const lockedLink = useInput("");
  const lockedDescription = useInput("");
  const description = useInput("");
  const [file, setFile] = useState("");
  const [fileData, setFileData] = useState("");
  const [contentCheck, setContentCheck] = useState(false);
  const [sensitiveCheck, setSensitiveCheck] = useState(false);

  const rout = useRouter();

  const switchCheckbox = () => {
    setContentCheck(!contentCheck);
  };
  const anotherSwitchCheckbox = () => {
    setSensitiveCheck(!sensitiveCheck);
  };
  const handleChange = (target) => {
    const reader = new FileReader();
    setFileData(target.files[0]);
    reader.readAsDataURL(target.files[0]);
    reader.onload = (e) => {
      const newUrl = e.target.result;
      setFile(newUrl);
    };

  };

  const getUrl = async (name) => await storageRef
    .ref()
    .child("items/" + name)
    .getDownloadURL()
    .then((url) => {
      return url
    })

  const submit = (e) => {
    e.preventDefault();
    setSending(true);
    const data = {
      author: user.userName,
      ownerUid: uid,
      name: name.value,
      link: link.value,
      lockedLink: lockedLink.value,
      description: description.value,
      lockedDescription: lockedDescription.value,
      lockedContent: contentCheck,
      sensitiveContent: sensitiveCheck,
      state: state.draft,
    }

    if (fileData) {
      storageRef.ref("items/" + fileData.name).put(fileData).then(() => {
        getUrl(fileData.name).then((url) => {
          db.collection("UserItems").add({
            ...data,
            image_url: url,
          })
            .then((res) => {
              setSuccess(true)
              setTimeout(() => {
                rout.push("/preview/" + res.id)
              }, 1500)
            })
            .catch(() => {
              setSending(false)
            })
        })
      });
    }
  };
  return (
    <section className="section page profile">
      <div className={"updated " + (success && "show")}>Item created successfully</div>
      <div className="container-big">
        <div className={css.wrapper}>
          <div className={css.block}>
            <div className={css.title}>Create New Item</div>
            <div className={css.info}>Image, Video, Audio, or 3D Model</div>
            <div className={css.fileTypes}>
              File types supported: JPG, PNG, GIF, SVG, MP4, WEBM, MP3, WAV,
              OGG, GLB, GLTF. Max size: 100 MB
            </div>
          </div>
          <form onSubmit={submit} className={css.form}>
            <div className={css.fileWrapper}>
              <label className={css.labelFile}>
                {file ? (
                  <img alt="file" className={css.imgFile} src={file} />
                ) : (
                  <img
                    className={css.fileImg}
                    src="/assets/img/icons/file-image.png"
                    alt="file"
                  />
                )}
                <input
                  required
                  accept=".png,.jpg,.gif,.svg,.mp4,.webm,.mp3,.wav,.ogg,.glb,.gltv,"
                  onChange={({ target }) => handleChange(target)}
                  className={css.inputFile}
                  type="file"
                />
              </label>
            </div>
            <label className={"input input-small profile-row " + css.label}>
              <span className="input__top">
                <span className="bd-font input__title">Title</span>
              </span>
              <input {...name} required type="text" className="input__field" />
            </label>
            <label className="input input-small profile-row">
              <span className="input__top d-flex align-items-center justify-content-between">
                <div>
                  <span className="bd-font input__title">Description</span>
                  <span className={"bd-font input__title " + css.labelDes}>
                    The description will be included on the item's detail page
                    underneath its image.
                  </span>
                </div>

                <span className="input__info">{description?.value?.length}/500</span>
              </span>
              <textarea
                required
                {...description}
                maxLength={500}
                name="label textarea"
                className="input__field textarea"
              ></textarea>
            </label>
            <label className={"input input-small profile-row " + css.label}>
              <span className="input__top">
                <span className="bd-font input__title">External Link</span>
                <span className={"bd-font input__title " + css.labelDes}>
                  NFTTRX will include a link to this URL on this item's detail
                  page, so that users can click to learn more about it. You are
                  welcome to link to your own webpage with more details.
                </span>
              </span>
              <input {...link} type="text" className="input__field" />
            </label>
            <label className={"input input-small profile-row " + css.label}>
              <span className="input__top d-flex align-items-center justify-content-between">
                <div>
                  <span className="bd-font input__title">
                    Unlockable Content
                  </span>
                  <span
                    className={
                      "bd-font input__title " +
                      css.labelDes +
                      " " +
                      css.checkboxDes
                    }
                  >
                    Include unlockable content that can only be revealed by the
                    owner of the item.
                  </span>
                </div>

                <span
                  className={
                    css.checkboxWrapper + " " + (contentCheck ? css.checked : "")
                  }
                >
                  <span
                    className={
                      css.checkbox + " " + (contentCheck ? css.checked2 : "")
                    }
                  ></span>
                  <input
                    onClick={switchCheckbox}
                    type="checkbox"
                    aria-label="Demo switch"
                    className={css.inputCheckbox}
                  />
                </span>
              </span>
            </label>
            <div className={css.line} />

            {
              contentCheck
                ? <>
                  <label className="input input-small profile-row mt-3">
                    <span className="input__top d-flex align-items-center justify-content-between">
                      <div>
                        <span className="bd-font input__title">Locked description </span>
                        <span className={"bd-font input__title " + css.labelDes}>
                          This part of description will be hidden from public.
                        </span>
                      </div>

                      <span className="input__info">{lockedDescription?.value?.length}/500</span>
                    </span>
                    <textarea
                      {...lockedDescription}
                      maxLength={500}
                      name="label textarea"
                      className="input__field textarea"
                    ></textarea>
                  </label>
                  <label className={"input input-small profile-row " + css.label}>
                    <span className="input__top">
                      <span className="bd-font input__title">Locked External Link (Optional)</span>
                      <span className={"bd-font input__title " + css.labelDes}>
                        This link will be hidden from public.
                      </span>
                    </span>
                    <input {...lockedLink} type="text" className="input__field" />
                  </label>
                </>
                : null
            }

            <label className={"input input-small profile-row " + css.label}>
              <span className="input__top d-flex align-items-center justify-content-between">
                <div>
                  <span className="bd-font input__title">
                    Explicit & Sensitive Content
                  </span>
                  <span
                    className={
                      "bd-font input__title " +
                      css.labelDes +
                      " " +
                      css.checkboxDes
                    }
                  >
                    Set this item as explicit and sensitive content
                  </span>
                </div>

                <span
                  className={
                    css.checkboxWrapper +
                    " " +
                    (sensitiveCheck ? css.checked : "")
                  }
                >
                  <span
                    className={
                      css.checkbox + " " + (sensitiveCheck ? css.checked2 : "")
                    }
                  ></span>
                  <input
                    onClick={anotherSwitchCheckbox}
                    type="checkbox"
                    aria-label="Demo switch"
                    className={css.inputCheckbox}
                  />
                </span>
              </span>
            </label>
            <div className={css.line} />
            <div className="d-flex justify-content-end mt-4">
              <ul className={css.ul}>
                <li className={css.closeBtn}>
                  <Link href="#">
                    <a>Cancel</a>
                  </Link>
                </li>
                <li>
                  <button
                    disabled={sending}
                    type="submit"
                    className="btn btn-small btn-solid-accent btn-round"
                  >
                    {
                      sending ? "Creating..." : "Create"
                    }
                  </button>
                </li>
              </ul>
            </div>
          </form>
        </div>
      </div>
    </section>
  );
}
