import Aside from "../../common/Aside";
import css from "../../../styles/transaction.module.css";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux"
import { Provider } from "../../../constants/wallet.js";
import Link from "next/link";
import { ethers } from "ethers";

export default function TransactionPage() {
  const rout = useRouter();
  const [isReceipt, setIsReceipt] = useState(null);
  const [transaction, setTransaction] = useState(null);
  console.log(transaction);
  const selectedNetwork = useSelector((state) => state.wallet.selectedNetwork)

  const getTransaction = () => {
    Provider(selectedNetwork).getTransactionReceipt(rout.query?.id)
      .then((res) => {
        setIsReceipt(!!res)
      });
  }

  useEffect(() => {
    const intervalId = setInterval(getTransaction, 10000)
    getTransaction()

    Provider(selectedNetwork).getTransaction(rout.query?.id)
      .then(async(res) => {
        const data = await ethers.providers.getNetwork(res.chainId);
        setTransaction({...res, network: data?.name})
      });

    return () => {
      clearInterval(intervalId)
    }
  }, []);

  return (
    <TransactionPageUI isSuccess={isReceipt} {...transaction} />
  )
}

function TransactionPageUI({ isSuccess, value, from, to, hash, blockNumber, network }) {
  const getEtherscan = `https://${network}.etherscan.io/tx/${hash}`

  const amount = value && ethers.utils.formatEther(value)
  return (
    <section className="section page profile">
      <div className="container-big">
        <div className="profile-row d-flex flex-wrap">
          <Aside />
          <div className="profile-content">
            <Link href="/wallet">
              <a className={css.arrow}>
                <img src="/assets/img/icons/arrow.png" />
              </a>
            </Link>
            <div
              className={
                css.block +
                " col-12 col-md-10 col-lg-8 d-flex flex-column align-items-center"
              }
            >
              <div className={css.title}>Transaction Details</div>
              <img className={css.img}
                src={"/assets/img/icons/" + (isSuccess ? "success.png" : "pending.png")}
              />
              <div className={css.check}>{
                isSuccess ? "Success" : "Pending"
              }</div>
              <div className={css.trx}>{amount} ETH</div>
              <div className={css.border}>
                <div className={css.underBorder}>
                  <div className={css.borderText}>Transfer account</div>
                  <div className={css.borderText2}>
                    {from}
                  </div>
                </div>
              </div>
              <div className={css.border}>
                <div className={css.underBorder}>
                  <div className={css.borderText}>Receiving account </div>
                  <div className={css.borderText2}>
                    {to}
                  </div>
                </div>
              </div>
              {/* <div className={css.border2}>
                <div className={css.underBorder}>
                  <div className={css.borderText}>Transaction type</div>
                  <div className={css.borderText2}>TRX Transfer</div>
                </div>
              </div> */}
              <div className={css.border3}>
                <div className={css.underBorder}>
                  <div className={css.borderText}>Transaction ID</div>
                  <div className={css.borderText2 + ' ' + css.break}>
                    <a className={css.link} target="_blank" href={getEtherscan}>
                      {hash}
                    </a>
                  </div>
                </div>
              </div>
              {/* <div className={css.border2}>
                <div className={css.underBorder}>
                  <div className={css.borderText}>Transaction time</div>
                  <div className={css.borderText2}>2021 - 11 -12   14:00:57</div>
                </div>
              </div> */}
              <div className={css.border2}>
                <div className={css.underBorder}>
                  <div className={css.borderText}>Block height</div>
                  <div className={css.borderText2}>{blockNumber}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
