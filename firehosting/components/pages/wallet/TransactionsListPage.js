import WalletHeader from "../../common/WalletHeader";
import WalletWrapper from "../../common/WalletWrapper";
import css from "../../../styles/transactions-list.module.css";
import { useEffect, useMemo, useState } from "react";
import { useRouter } from "next/router";
import { useMoralisWeb3Api } from "react-moralis";
import { useSelector } from "react-redux";
import { Provider } from "../../../constants/wallet.js";
import Link from "next/link"

export default function TransactionsListPage({ isNft }) {
  const {
    selectedWallet,
    selectedNetwork,
    tokensData,
    nftsData
  } = useSelector((state) => state.wallet);

  const [list, setList] = useState([]);
  const { query, push } = useRouter();

  const [selectedToken] = tokensData.filter((item) => item.token_address === query.id);
  const [selectedNft] = nftsData?.filter((item) => item.token_address === query.id);

  const Web3Api = useMoralisWeb3Api();

  const options = {
    chain: selectedNetwork,
    address: selectedWallet.Address,
  };

  const getNftsTransactions = async () => {
    await Provider(selectedNetwork);
    const res = await Web3Api.account.getNFTTransfers(options);
    const filteredList = res.result?.filter((token) => token.token_address === query.id)
    setList(filteredList)
  }

  const getTransactions = async () => {
    await Provider(selectedNetwork);
    const res = await Web3Api.account.getTokenTransfers(options);
    const filteredList = res.result?.filter((token) => token.address === query.id)
    setList(filteredList)
  }

  const drawTransactionList = useMemo(() => list.map((el) => {
    const amount = el.amount || el.value / Math.pow(10, 18);
    const total = el.to_address !== selectedWallet.Address.toLowerCase()
      ? "-" + amount
      : amount
    return (
      <ListItem
        key={el.block_timestamp}
        name={el.from_address}
        time={el.block_timestamp}
        value={total}
      />
    )
  }), [list])

  useEffect(() => {
    if (isNft) {
      getNftsTransactions();
    } else {
      getTransactions();
    }
  }, [selectedWallet]);

  useEffect(() => {
    if (isNft && !selectedNft) {
      push("/wallet")
    }
    if (!isNft && !selectedToken) {
      push("/wallet")
    }
  }, []);

  return (
    <WalletWrapper>
      {
        isNft
          ? <TransactionListUi list={drawTransactionList} isNft {...selectedNft} />
          : <TransactionListUi list={drawTransactionList} {...selectedToken} />
      }
    </WalletWrapper>
  );
}

export const TransactionListUi = (
  { isNft, list, symbol, image_url, realBalance, name, amount, token_id, metadata, token_address, ...props }
) => {
  const data = metadata && JSON.parse(metadata)
  return (
    <>
      <WalletHeader title={symbol + " Transactions List"} />
      <div className="d-flex align-items-center flex-column">
        <img
          className={isNft ? css.nftImage : css.img}
          src={image_url || "/assets/img/icons/empty-token.webp"}
        />
        <div className={css.text}>{realBalance || amount}</div>
        <div className={css.sub_title}>{name}</div>
        <div className={css.sub_title + " mt-3"}>{data?.description}</div>
        <div className={css.borderWrapper}>
          <div className={css.border}>
            <div className={css.text}>{realBalance || amount}</div>
            <div className={css.sub_title}>{realBalance ? "Available balance" : "Amount"}</div>
          </div>
          <div className={css.border}>
            <div className={css.text}>{token_id || "0"}</div>
            <div className={css.sub_title}>{token_id ? "Token ID" : "Staked balance"}</div>
          </div>{" "}
        </div>
        <div
          className={
            css.tabs + " d-flex justify-content-between align-items-center"
          }
        >
          <div className={css.nav + " d-flex align-items-center"}>
            <a href="#" className={css.active}>TRX</a>
          </div>
        </div>
        {list}
        <div className={css.wrapper + " mt-5"}>
          <br />
          <Link href={
            "/wallet/send" + (
              isNft
                ? "-nft/" + token_id
                : "-token/" + token_address
            )
          }>
            <button className={css.btn + " btn btn-fill-accent main-btn-radius protection"}>
              <a className={css.action}>
                <img src="/assets/img/icons/arrow-up-circle.png" alt="SEND" />
                <p>Send</p>
              </a>
            </button>
          </Link>
          <button className={css.btn + " btn btn-fill-accent main-btn-radius protection"}>
            <a href="#" className={css.action}>
              <img className={css.receive} src="/assets/img/icons/arrow-up-circle.png" alt="RECEIVE" />
              <p>Receive</p>
            </a>
          </button>
        </div>
      </div>
    </>
  )
}

export const ListItem = (props) => {
  return (
    <div
      className={
        css.listItem + " " + css.lastItem + " d-flex justify-content-between "
      }
    >
      <div>
        <div className={css.text + " " + css.dop}>{props.name}</div>
        <div className={css.sub_title + " " + css.dop2}>{props.time}</div>
      </div>
      <div className={props.value > 0 ? css.plus : css.minus}>{props.value > 0 ? "+" : ""}{props.value}</div>
    </div>
  );
};
