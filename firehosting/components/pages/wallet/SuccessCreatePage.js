import WalletWrapper from "../../common/WalletWrapper.js";
import WalletHeader from "../../common/WalletHeader.js";
import css from '../../../styles/success-import.module.css';

export default function SuccessCreatePage() {
    return (
        <WalletWrapper>
            <WalletHeader title="Create Wallet" />
            <div className='d-flex flex-column align-items-center'>
               <div className={css.block}>
            </div>
            <img className={css.img} src='/assets/img/icons/success.png'/>
                <div className={css.text}>Successfully created</div> 
            <div className="profile-bottom d-flex align-items-center justify-content-center justify-content-md-end">
                  <button  className="btn btn-round btn-middle btn-solid-accent profile-bottom__save">Let's review it</button>
                </div>
            </div>
            
        </WalletWrapper>
    )
}
