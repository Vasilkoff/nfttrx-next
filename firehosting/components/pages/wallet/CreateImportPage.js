import Link from "next/link";
import WalletCard from "../../common/walletCard/WalletCard.js";
import WalletWrapper from "../../common/WalletWrapper.js";

export default function CreateImportPage() {
    return (
        <WalletWrapper>
            <WalletCard isImport />
            <div
                className={`d-flex justify-content-center mt-4`}
            >
                <Link href="/wallet/create">
                    <a className="btn btn-fill-accent main-btn-radius mr-2">Create wallet</a>
                </Link>
                <Link href="/wallet/import">
                    <a className="btn btn-fill-accent main-btn-radius ml-2">Import wallet</a>
                </Link>
            </div>
        </WalletWrapper>
    )
}
