import WalletWrapper from "../../common/WalletWrapper.js";
import WalletHeader from "../../common/WalletHeader.js";
import { useCallback, useState } from "react";
import { db } from "../../../config/firebase.js";
import { useSelector } from "react-redux";
import { ethers } from "ethers";
import { Provider } from "../../../constants/wallet.js";
import Router from "next/router";

export default function ImportWalletPage() {
  const [name, setName] = useState("");
  const [key, setKey] = useState("");
  const [error, setError] = useState("");
  const uid = useSelector((state) => state.auth.data?.uid);
  const WalletLength = useSelector((state) => state.wallet.data.length + 1);
  const defaultName = "Wallet " + WalletLength;

  const handleImport = useCallback(async (e) => {
    e.preventDefault();
    if (key.charAt(0) != "0" && key.charAt(1) != "x") {
      setKey("0x" + key);
    }
    const snapCheckName = await db
      .collection("Users")
      .doc(uid)
      .collection("wallet")
      .where("name", "==", name || defaultName)
      .get();
    if (snapCheckName.empty) {
      const snapCheckKey = await db
        .collection("Users")
        .doc(uid)
        .collection("wallet")
        .where("PrivateKey", "==", key)
        .get();
      console.log(snapCheckKey);
      if (snapCheckKey.empty) {
        try {
          // it could be ethers.Wallet.fromMnemonic( mnemonic );
          let wallet = new ethers.Wallet(key, Provider('ropsten'));
          wallet.balance = await Provider().getBalance(wallet.address);
          wallet.balance = ethers.utils.formatEther(wallet.balance);
          let doc = db.collection("Users").doc(uid).collection("wallet").doc();
          doc
            .set({
              OwnerUID: uid,
              Mnemonic: "NA (imported by key)",
              WalletID: name || defaultName,
              Address: wallet.address,
              PrivateKey: wallet.privateKey,
              PublicKey: wallet.publicKey,
              name: name || defaultName,
              balanceEth: wallet.balance,
              timestamp: new Date(),
            })
            .then(() => {
              Router.push("/wallet/import-success");
            });
        } catch (err) {
          console.log(err);
          setError("Import error: " + err.message);
        }
      } else {
        setError("You already have a wallet with the specified key!");
      }
    } else {
      setError(
        "You already have a wallet with this name! Please, think out another one"
      );
    }
  }, [uid, key]);
  return (
    <WalletWrapper>
      <WalletHeader title="Import Wallet" />
      <form onSubmit={handleImport} className="mt-5">
        <label className="input input-small profile-row">
          <span className="input__top">
            <span className="bd-font input__title">
              Please, give your wallet a name
            </span>
          </span>
          <input
            type="text"
            name="name"
            className="input__field"
            placeholder={defaultName}
            value={name}
            onChange={({ target }) => setName(target.value)}
          />
        </label>
        <label className="input input-small profile-row">
          <span className="input__top">
            <span className="bd-font input__title">Eth wallet private key</span>
          </span>
          <input
            type="text"
            name="key"
            className="input__field"
            required
            value={key}
            onChange={({ target }) => setKey(target.value)}
          />
        </label>
        <button className="ml-auto d-block btn btn-solid-accent main-btn-radius">
          Import
        </button>
        <div className="error-message">{error}</div>
      </form>
    </WalletWrapper>
  );
}
