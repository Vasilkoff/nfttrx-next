import WalletWrapper from "../../common/WalletWrapper.js";
import WalletHeader from "../../common/WalletHeader.js";
import Router from "next/router";
import useInput from "../../../hooks/useInput"
import { Provider } from "../../../constants/wallet.js";
import { useSelector } from "react-redux";
import { ethers } from "ethers";
import { useState } from "react";
import { useRouter } from "next/dist/client/router";
import { db } from './../../../config/firebase';
import { getAuth } from './../../../store/selectors';

export default function SendPage({ isNft, isToken }) {
  const { query } = useRouter();
  const userItems = useSelector((state) => state.items?.userItems)

  const { selectedNetwork, selectedWallet, token, nft } = useSelector((state) => {
    const [token] = state.wallet.tokensData.filter((token) => token.token_address === query.id);
    const [nft] = state.wallet.nftsData.filter((token) => token.token_id === query.id);
    const data = {
      selectedNetwork: state.wallet.selectedNetwork,
      selectedWallet: state.wallet.selectedWallet,
      token,
      nft
    }
    return data;
  })

  console.log(nft);

  const account = useInput("");
  const amount = useInput(nft?.token_id || "");
  const [sending, setSending] = useState(false);
  const [errorMessage, setErrorMessage] = useState("")


  const sendEth = async () => {
    setSending(true)
    const wallet = new ethers.Wallet(selectedWallet.PrivateKey, Provider(selectedNetwork));

    const tx = {
      to: account.value,
      value: amount.value && ethers.utils.parseEther(amount.value),
    }

    wallet.sendTransaction(tx)
      .then((res) => {
        Router.push("/wallet/transaction/" + res.hash);
      })
      .catch(err => {
        setSending(false)
        console.log(err);
        setErrorMessage("Something went wrong! Please try again later.")
      })
  }

  const sendToken = async () => {
    setSending(true)
    const wallet = new ethers.Wallet(selectedWallet.PrivateKey, Provider(selectedNetwork));

    const tx = {
      to: account.value,
      value: amount.value && ethers.utils.parseEther(amount.value),
    }
    const abi = [
      "function transfer(address _to, uint256 _value) public returns (bool success)",
    ];
    const erc20_rw = new ethers.Contract(token.token_address, abi, wallet);

    erc20_rw.transfer(tx.to, ethers.utils.parseUnits(amount.value))
      .then((res) => {
        Router.push("/wallet/transaction/" + res.hash);
      })
      .catch(err => {
        setSending(false)
        setErrorMessage("Somethin went wrong!!!")
      })
  }

  const sendNft = async () => {
    setSending(true)

    async function getGasPrice() {
      let feeData = await Provider(selectedNetwork).getFeeData()
      return feeData.gasPrice
    }

    async function getNonce(signer) {
      return (await signer).getTransactionCount()
    }
    const wallet = new ethers.Wallet(selectedWallet.PrivateKey, Provider(selectedNetwork));

    const tx = {
      to: account.value,
      value: amount.value && ethers.utils.parseEther(amount.value),
    }
    const abi1155 = [
      "function safeTransferFrom(address _from, address _to, uint256 _id, uint256 _value, bytes calldata _data) external",
    ];
    const abi721 = [
      "function safeTransferFrom(address _from, address _to, uint256 _tokenId, bytes data) external payable",
    ]
    const nonce = await getNonce(wallet)
    const gasFee = await getGasPrice()

    const removeNftFromUser = () => {
      const [sendingNft] = userItems.filter((item) => item?.token_address === nft?.token_address)
      db.collection("UserItems").doc(sendingNft?.id).set({ownerUid: ""}, {merge: true})
    }
    if (nft?.contract_type === "ERC721") {
      const erc20_rw = new ethers.Contract(nft.token_address, abi721, wallet);
      const rawTxn = await erc20_rw.populateTransaction.safeTransferFrom(selectedWallet.Address, tx.to, nft.token_id, [], {
        gasPrice: gasFee,
        nonce: nonce,
        gasLimit: ethers.utils.hexlify(210000)
      })
      if (rawTxn) {
        removeNftFromUser();
        const signedTxn = await wallet.sendTransaction(rawTxn)
        Router.push("/wallet/transaction/" + signedTxn.hash);
      } else {
        setErrorMessage("Something went wrong! Please try later.");
        setSending(false);
      }
    }
    if (nft?.contract_type === "ERC1155") {
      const erc20_rw = new ethers.Contract(nft.token_address, abi1155, wallet);
      const rawTxn = await erc20_rw.populateTransaction.safeTransferFrom(selectedWallet.Address, tx.to, nft.token_id, nft?.amount, [], {
        gasPrice: gasFee,
        nonce: nonce,
        gasLimit: ethers.utils.hexlify(210000)
      })
      if (rawTxn) {
        removeNftFromUser();
        const signedTxn = await wallet.sendTransaction(rawTxn)
        Router.push("/wallet/transaction/" + signedTxn.hash);
      } else {
        setErrorMessage("Something went wrong! Please try later.");
        setSending(false);
      }
    }
  }

  const handleSend = (e) => {
    e.preventDefault();
    if (isToken) {
      sendToken()
    } else if (isNft) {
      sendNft()
    } else {
      sendEth()
    }
  }

  return (
    <WalletWrapper>
      <WalletHeader title={"Send"} name={token?.name || nft?.name} />
      <form onSubmit={handleSend} className="mt-5">
        <label className="input input-small profile-row">
          <span className="input__top">
            <span className="bd-font input__title">Receiving account</span>
          </span>
          <input {...account} type="text" name="name" className="input__field" required />
        </label>
        <label className="input input-small profile-row">
          <span className="input__top">
            <span className="bd-font input__title">Transfer amount</span>
          </span>
          <input disabled={isNft} {...amount} type="number" name="key" className="input__field" required />
        </label>
        <button
          disabled={sending}
          className="ml-auto d-block btn btn-solid-accent main-btn-radius"
        >
          {sending ? "Sending..." : "Send"}
        </button>
        <div className="error-message">{errorMessage}</div>
      </form>
    </WalletWrapper>
  )
}
