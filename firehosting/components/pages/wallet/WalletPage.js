import WalletCard from "../../common/walletCard/WalletCard.js";
import WalletList from "../../common/walletList/WalletList.js";
import WalletWrapper from "../../common/WalletWrapper.js";
import { useMoralisWeb3Api } from "react-moralis";
import { useEffect } from "react";
import { setNfts, setSelectedWallet, setTokens } from "../../../store/actions/walletAction.js";
import { useDispatch, useSelector } from "react-redux";
import WalletHeader from "../../common/walletHeader/WalletHeader.js";
import { ethers } from "ethers";
import { Provider } from "../../../constants/wallet.js";
import { db } from "../../../config/firebase.js";


export default function WalletPage() {
  const { selectedWallet, selectedNetwork } = useSelector((state) => state.wallet)
  const uid = useSelector((state) => state.auth?.data?.uid);
  const Web3Api = useMoralisWeb3Api();
  const dispatch = useDispatch();

  const options = {
    chain: selectedNetwork,
    address: selectedWallet?.Address,
  };

  const getNFTs = async () => {
    const nfts = await Web3Api.account.getNFTs(options);
    const result = nfts.result.map((token) => {
      const { image } = JSON.parse(token.metadata);
      const id = image.split("//")[1];
      const baseUrl = token.token_uri.split("/").splice(0, 4).join("/");
      const image_url = baseUrl + "/" + id;
      return {
        ...token,
        image_url: token.contract_type === "ERC721" ? image_url : image
      }
    })
    return result;
  };

  const getTokens = async () => {
    const results = await Web3Api?.account?.getTokenBalances(options);
    const tokens = results.map((token) => {
      return {
        ...token,
        realBalance: token.balance / Math.pow(10, +token?.decimals)
      }
    });
    return tokens;
  };

  const fetchData = async () => {
    try {
      const balance = await Provider(selectedNetwork).getBalance(selectedWallet.Address);
      const balanceEth = ethers.utils.formatEther(balance);
      db.collection("Users").doc(uid).collection("wallet").doc(selectedWallet?.id)
        .update({
          ["balance" + selectedNetwork]: balanceEth
        })
        .catch((error) => {
          console.log(error);
        })
      dispatch(setSelectedWallet({
        ...selectedWallet,
        ["balance" + selectedNetwork]: balanceEth
      }));
    }
    catch (error) {
      console.log(error);
    }
  };

  const fetchTokensNfts = async () => {
    const tokens = await getTokens();
    dispatch(setTokens(tokens))
    const nfts = await getNFTs(selectedWallet?.Address);
    dispatch(setNfts(nfts))
  };

  useEffect(() => {
    if (selectedWallet) {
      fetchData()
    }
  }, [selectedNetwork]);

  useEffect(() => {
    if (selectedWallet) {
      fetchTokensNfts()
    }
  }, [selectedNetwork, selectedWallet]);

  return (
    <WalletWrapper>
      <WalletHeader />
      <WalletCard />
      <WalletList />
    </WalletWrapper>
  )
}
