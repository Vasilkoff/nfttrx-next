import css from "../../../styles/qr.module.css";
import WalletHeader from "../../common/WalletHeader.js";
import WalletWrapper from "../../common/WalletWrapper.js";
import { useSelector } from "react-redux";
import { useState } from "react";

export default function QrPage() {
  const wallets = useSelector((state) => state.wallet.data);
  const [wallet] = wallets;
  const [isUpdated, setIsUpdated] = useState(false);

  const updateSuccess = () => {
    setIsUpdated(true);
    setTimeout(() => setIsUpdated(false), 5000);
  };
  function copyKey() {
    navigator.clipboard.writeText(wallet?.PrivateKey);
    updateSuccess()
  }
  return (
    <>
      <div className={css.updated + " " + (isUpdated && css.show)}>
        Copied to clipboard
      </div>
      <WalletWrapper>
        <WalletHeader title="TRX Receive" />
        <div className={css.block + " mt-5 d-flex "}>
          <div className={css.qrWrapper}>
            {" "}
            <div className={css.qr}>
              <img src="/assets/img/icons/qr.png" />
            </div>
          </div>
          <div className={css.text}>
            You can copy the wallet address from <br /> another device using a
            QR code reader.
          </div>
          <div className={css.text2}>{wallet?.PrivateKey}</div>
          <div
            onClick={copyKey}
            class={
              css.btn +
              " btn btn-round btn-solid-accent screen__sell d-flex justify-content-around align-items-center"
            }
          >
            Copy account{" "}
            <img className={css.btnImg} src="/assets/img/icons/copy.png" />
          </div>
        </div>
      </WalletWrapper>
    </>
  );
}
