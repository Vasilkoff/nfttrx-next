import { useState } from "react";
import WalletWrapper from "../../common/WalletWrapper.js";
import WalletHeader from "../../common/WalletHeader.js";
import { db } from "../../../config/firebase.js";
import { useSelector } from "react-redux";
import { ethers } from "ethers";
import Router from "next/router";

export default function CreateWalletPage() {
  const uid = useSelector((state) => state.auth.data?.uid);
  const WalletLength = useSelector((state) => state.wallet.data.length + 1);
  const defaultName = "Wallet " + WalletLength;
  const [name, setName] = useState('');
  const [error, setError] = useState('');
  const [isSending, setIsSending] = useState(false);

  const handleCreate = async (e) => {
    setIsSending(true)
    e.preventDefault();

    const snapCheck = await db.collection("Users").doc(uid).collection("wallet").where("name", "==", name || defaultName).get();
    if (snapCheck.empty) {
      setError('');
      let wallet = ethers.Wallet.createRandom();

      let doc = db.collection("Users").doc(uid).collection('wallet').doc();
      let docId = doc.id;
      doc.set({
        WalletID: docId,
        Address: wallet.address,
        PrivateKey: wallet.privateKey,
        PublicKey: wallet.publicKey,
        OwnerUID: uid,
        Mnemonic: wallet.mnemonic.phrase,
        name: name || defaultName,
        timestamp: new Date()
      })
        .finally(() => setIsSending(false))
        .then(() => {
          Router.push("/wallet/create-success")
        })
        .catch((error) => {
          setError("Error creating new wallet record: " + error);
        });
    } else {
      setIsSending(false)
      setError('You already have a wallet with the name entered! Please, think out another one');
    }

  }
  return (
    <WalletWrapper>
      <WalletHeader title="Create Wallet" />
      <form onSubmit={handleCreate} className="mt-5">
        <label className="input input-small profile-row">
          <span className="input__top">
            <span className="bd-font input__title">Please, give your wallet a name</span>
          </span>
          <input
            type="text"
            name="name"
            className="input__field"
            placeholder={"Wallet " + WalletLength}
            value={name}
            onChange={({ target }) => setName(target.value)}
          />
        </label>
        <button disabled={isSending} className="ml-auto d-block btn btn-solid-accent main-btn-radius">Create</button>
        <div className="error-message">{error}</div>
      </form>
    </WalletWrapper>
  )
}
