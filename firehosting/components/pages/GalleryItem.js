import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { db } from "../../config/firebase";
import { Networks } from "../../constants/wallet";
import css from "../../styles/galleryItem.module.css";
import Preloader from "../common/preloader/Preloader";
import Api, { BASE_URL } from "../../api/Api";
import Link from "next/link";

export default function GalleryItem() {
  const [pending, setPending] = useState(true);
  const [data, setData] = useState(null);
  const [owner, setOwner] = useState({});
  const [priceId, setPriceId] = useState("");
  const [location, setLocation] = useState("");

  const userData = useSelector((state) => state.auth.data);
  const uid = userData?.uid;
  const user = userData?.user;
  const router = useRouter();
  const { id, success } = router.query;

  useEffect(() => {
    if (window) {
      setLocation(window.location.href);
      if (success) {
        // alert("Item bought successfully!")
      }
    }
    db.collection("UserItems")
      .doc(id)
      .get()
      .finally(() => setPending(false))
      .then((res) => {
        console.log(res.data());
        setData(res.data());
        db.collection("Users")
          .doc(res.data().ownerUid)
          .get()
          .then((r) => {
            setOwner(r.data());
          });
      });
  }, []);

  const setPriceIdToItem = (pId) => {
    setPriceId(pId);
    db.collection("UserItems").doc(id).update({ priceId: pId });
  };

  useEffect(() => {
    if (data && !data?.priceId) {
      Api.getPriceId(data.name, data.price * 100, data.priceCurrency).then(
        (res) => {
          console.log(res.data);
          if (res?.data) {
            setPriceIdToItem(res.data.id);
          }
        }
      );
    }
  }, [data]);

  if (pending || !data) return <Preloader />;
  const {
    image_url,
    author,
    name,
    description,
    price,
    priceCurrency,
    blockchain,
    ownerUid,
    ...props
  } = data;
  const { avatar } = owner;
  const isMyItem = ownerUid === uid;
  const meta = props?.metadata && JSON.parse(props?.metadata);
  return (
    <div className="row">
      <div
        className={
          css.left +
          " row col-md-6  col-12 align-items-center justify-content-center"
        }
      >
        <div className={css.leftBlock + " " + "col-10 col-md-9"}>
          <img
            className={css.leftImg}
            src={image_url || "/assets/img/example/gallery/2fullSize.jpg"}
          />
        </div>
        <div
          className={
            css.leftFooter + " row col-10 col-md-9 justify-content-center"
          }
        >
          {/* <a className=" d-flex">
            <img
              className={css.union}
              src="/assets/img/example/gallery/Union.png"
            />
            <span className={css.share}>Share</span>
          </a> */}
        </div>
      </div>

      {/* RIGHT BLOCK  */}
      <div className="col-md-6 col-12">
        <div className={css.right + " row  justify-content-center"}>
          <div className={css.rightBlock + " col-10 col-md-9"}>
            <br />
            <div className="d-flex align-items-center">
              <div className={css.userWrapper}>
                <img
                  className={css.user}
                  src={avatar || "/assets/img/icons/avatars.svg"}
                />
              </div>
              <div>
                Owned by{" "}
                <Link href={`user/${ownerUid}`}>
                  <a className={css.lebedova}>
                    @{author ? author : ""}
                  </a>
                </Link>
              </div>
            </div>
            <div className={css.textsWrapper + " row"}>
              <div className={css.text + " col-8"}>{name}</div>
              <div className={css.text2 + " col-11 col-md-10"}>
                {description || meta?.description}
              </div>
            </div>
          </div>
          <div className={css.rightFooter + " col-10 col-md-9"}>
            <div className={css.line + " col-11"}></div>
            <div className="row align-items-center">
              {priceCurrency === "GBP" ? (
                <span className="mr-1">£</span>
              ) : (
                <img
                  src={Networks[blockchain]?.image || Networks.ropsten?.image}
                  className="square-currency__icon"
                />
              )}
              <div className={css.price + " ml-2"}>{price}</div>
              {/* <div className={css.price2}>( $ 1040.09 )</div> */}
              {isMyItem ? (
                <a className={css.btnWrapper + " col-11"}>
                  This is your nft already!
                </a>
              ) : (
                <a className={css.btnWrapper + " col-11"}>
                  <form
                    method="POST"
                    action={
                      BASE_URL + "paymentsession/" +
                      (priceId || data.priceId)
                    }
                  >
                    <input
                      type="hidden"
                      value={location + "?success=true"}
                      name="success_url"
                    />
                    <input
                      type="hidden"
                      value={location + "?canceled=true"}
                      name="cancel_url"
                    />
                    <input type="hidden" value={ownerUid} name="fromID" />
                    <input type="hidden" value={uid} name="toID" />
                    <input type="hidden" value={id} name="itemID" />
                    <input type="hidden" value={user?.userName} name="author" />
                    <input type="hidden" value={name} name="name" />
                    <button
                      type="submit"
                      className={
                        css.btn + " btn btn-fill-accent main-btn-radius"
                      }
                    >
                      Buy NFT
                    </button>
                  </form>
                </a>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
