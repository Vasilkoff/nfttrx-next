import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import 'firebase/compat/storage';

const firebaseConfig = {
  apiKey: "AIzaSyA12AVUCkeFtpuVJLHmntowmSGxOWEQdfk",
  authDomain: "nfttrx.firebaseapp.com",
  databaseURL: "https://nfttrx-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "nfttrx",
  storageBucket: "nfttrx.appspot.com",
  messagingSenderId: "51530052551",
  appId: "1:51530052551:web:2a51de69eba308c08d9fe2"
};

try {
  firebase.initializeApp(firebaseConfig);
} catch(err){
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack)}
}
export const db = firebase.firestore();
export const storageRef = firebase.storage();

const fire = firebase;
export default fire;