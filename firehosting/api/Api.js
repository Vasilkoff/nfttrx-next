import axios from "axios";

export const BASE_URL = "https://us-central1-nfttrx.cloudfunctions.net/v1/";
const http = axios.create({
  baseURL: BASE_URL,
});

const Api = {
  getNewToken: (uid = "", sec = 600, name = "basic-kyc-level") =>
    http.get(`everything/${uid}/${sec}/${name}/`),
  getPdfLink: (docId) => http.get(`agreement/pdf/${docId}/`),
  getPriceId: (product, amount, currency) =>
    http.post(`price/${product}/${amount}/${currency}`),
  getPublicStripeKey: () => http.get("public-key"),
  getProductDetails: () => http.get("product-details"),
  createPaymentIntent: (options) => http.post("create-payment-intent", options)
};

export default Api;