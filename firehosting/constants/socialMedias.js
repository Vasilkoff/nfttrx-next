const socialMedias = [
    {
        img: "/assets/img/icons/facebook.png",
        name: "facebook",
        pattern: /^(https?:\/\/){0,1}(www\.){0,1}facebook\.com\/(?:\w)/,
    },
    {
        img: "/assets/img/icons/in.png",
        name: "linkedin",
        pattern: /^(https?:\/\/){0,1}(www\.){0,1}linkedin\.com\/(?:\w)/,
    },
    {
        img: "/assets/img/icons/twitter.png",
        name: "twitter",
        pattern: /^(https?:\/\/){0,1}(www\.){0,1}twitter\.com\/(?:\w)/,
    },
    {
        img: "/assets/img/icons/insta.png",
        name: "instagram",
        pattern: /^(https?:\/\/){0,1}(www\.){0,1}instagram\.com\/(?:\w)/,
    },
    {
        img: "/assets/img/icons/discord.png",
        name: "discord",
        pattern: /^(https?:\/\/){0,1}(www\.){0,1}discord\.com\/(?:\w)/,
    },
    {
        img: "/assets/img/icons/behance.png",
        name: "behance",
        pattern: /^(https?:\/\/){0,1}(www\.){0,1}behance\.net\/(?:\w)/,
    },
    {
        img: "/assets/img/icons/dribbble.png",
        name: "dribbble",
        pattern: /^(https?:\/\/){0,1}(www\.){0,1}dribbble\.com\/(?:\w)/,
    },
]
export default socialMedias