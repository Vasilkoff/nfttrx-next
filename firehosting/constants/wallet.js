import { ethers } from "ethers";

// RPC 
//https://speedy-nodes-nyc.moralis.io/33f6f8d07a3a20b5bba9ed87/eth/ropsten
//https://speedy-nodes-nyc.moralis.io/33f6f8d07a3a20b5bba9ed87/eth/mainnet
export const NODE_URL = "https://speedy-nodes-nyc.moralis.io/33f6f8d07a3a20b5bba9ed87/eth/ropsten";

const speedyNodeKey = "33f6f8d07a3a20b5bba9ed87";
export const Networks = {
  "keys": {
    1: `https://speedy-nodes-nyc.moralis.io/${speedyNodeKey}/eth/mainnet`,
    3: `https://speedy-nodes-nyc.moralis.io/${speedyNodeKey}/eth/ropsten`,
    4: `https://speedy-nodes-nyc.moralis.io/${speedyNodeKey}/eth/rinkeby`,
    5: `https://speedy-nodes-nyc.moralis.io/${speedyNodeKey}/eth/goerli`,
    42: `https://speedy-nodes-nyc.moralis.io/${speedyNodeKey}/eth/kovan`,
    137: `https://speedy-nodes-nyc.moralis.io/${speedyNodeKey}/polygon/mainnet`,
    80001: `https://speedy-nodes-nyc.moralis.io/${speedyNodeKey}/polygon/mumbai`,
    56: `https://speedy-nodes-nyc.moralis.io/${speedyNodeKey}/bsc/mainnet`,
    97: `https://speedy-nodes-nyc.moralis.io/${speedyNodeKey}/bsc/testnet`,
    43114: `https://speedy-nodes-nyc.moralis.io/${speedyNodeKey}/avalanche/mainnet`,
    43113: `https://speedy-nodes-nyc.moralis.io/${speedyNodeKey}/avalanche/testnet`,
    250: `https://speedy-nodes-nyc.moralis.io/${speedyNodeKey}/fantom/mainnet`,
  },
  // Urgant! Ropsten should be in the first place.  
  "ropsten": {
    "image": "/assets/img/icons/empty-token.webp",
    "EtherscanAddress": "https://ropsten.etherscan.io/address/",
    "nodeURL": "https://speedy-nodes-nyc.moralis.io/33f6f8d07a3a20b5bba9ed87/eth/ropsten",
    "key": "ETH",
  },
  "rinkeby": {
    "image": "/assets/img/icons/empty-token.webp",
    "EtherscanAddress": "https://rinkeby.etherscan.io/address/",
    "nodeURL": "https://speedy-nodes-nyc.moralis.io/33f6f8d07a3a20b5bba9ed87/eth/rinkeby",
    "key": "ETH",
  },
  "eth": {
    "image": "/assets/img/icons/Ethereum.png",
    "EtherscanAddress": "https://etherscan.io/address/",
    "nodeURL": "https://speedy-nodes-nyc.moralis.io/33f6f8d07a3a20b5bba9ed87/eth/mainnet",
    "key": "ETH",
  },
  "bsc": {
    "image": "/assets/img/icons/BNB.png",
    "EtherscanAddress": "https://bscscan.com/address/",
    "nodeURL": "https://speedy-nodes-nyc.moralis.io/33f6f8d07a3a20b5bba9ed87/bsc/mainnet",
    "key": "BNB",
  },
  "polygon": {
    "image": "/assets/img/icons/Polygon.png",
    "EtherscanAddress": "https://polygonscan.com/address/",
    "nodeURL": "https://speedy-nodes-nyc.moralis.io/33f6f8d07a3a20b5bba9ed87/polygon/mainnet",
    "key": "MATIC",
  },
};

export const Provider = (key = "eth") => new ethers.providers.JsonRpcProvider(Networks[key].nodeURL);
