import { monthes } from "../constants";

export const getCurrentDateDMY = () => {
  const date = new Date().toLocaleDateString().split("/");
  const toDay = date[1] < 10 ? "0" + date[1] : date[1];
  const month = monthes[date[0] - 1];
  const year = date[2];
  const newDate = `${toDay} ${month} ${year}`;
  return newDate;
};
