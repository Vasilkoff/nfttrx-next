import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { setRegisterStep } from "../store/actions/authAction.js";
import { getAuth, getAuthStatus } from "../store/selectors.js";

const WithAuth = ({ Component }) => {
  const route = useRouter();
  const authData = useSelector(getAuth);
  const authStatus = useSelector(getAuthStatus);
  const isAuth = Boolean(authStatus);
  const isAuthData = Boolean(authData);
  const dispatch = useDispatch();

  if (authData?.user?.allRegisterStepsDone) {
    if (
      !authData?.user?.isAgencyAgreement &&
      !authData?.user?.isAgencyAgreementCanceled
    ) {
      if (route.pathname === "/agency-agreement") {
        return <Component />;
      }
      route.push("/agency-agreement");
      return null;
    } else {
      if (route.pathname === "/agency-agreement") {
        route.push("/profile");
        return null;
      }
      return <Component />;
    }
  } else if (!isAuth && isAuthData) {
    route.push("/register");
    return null;
  } else if (
    isAuth &&
    (authData?.user.kycVerificationCanceled)
  ) {
    if (authData?.user?.accountFee && !authData?.user.kycVerificationCanceled) {
      route.push("/register");
      dispatch(setRegisterStep(4));
    }
    return <Component />;
  } else if (isAuth && isAuthData) {
    route.push("/register");
    dispatch(setRegisterStep(3));
    return null;
  } else {
    route.push("/login");
    return null;
  }
};

export default WithAuth;
