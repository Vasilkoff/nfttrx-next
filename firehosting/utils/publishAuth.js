import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { setRegisterStep } from "../store/actions/authAction.js";
import { getAuth, getAuthStatus } from "../store/selectors.js";

const WithNotAuth = ({ Component }) => {
  const route = useRouter();
  const authData = useSelector(getAuth);
  const authStatus = useSelector(getAuthStatus);
  const isAuth = Boolean(authData);
  const dispatch = useDispatch();

  if(authData?.user?.allRegisterStepsDone) {
    if(!authData?.user?.isAgencyAgreement && !authData?.user?.isAgencyAgreementCanceled) {
      route.push("/agency-agreement")
      return null;
    }else {
      route.push("/profile")
      return null;
    }
  } else if (isAuth && !authStatus && route.pathname !== "/register") {
    route.push("/register");
    dispatch(setRegisterStep(2));
    return null;
  } else if (!isAuth || route.pathname === "/register") {
    if (authData?.user?.kycVerificationCanceled) {
      route.push("/profile");
      return null;
    } else if (authData?.user?.accountFee) {
      dispatch(setRegisterStep(4));
      return <Component />;
    } else {
      return <Component />;
    }
  } else {
    route.push("/profile");
    return null;
  }
};

export default WithNotAuth;
