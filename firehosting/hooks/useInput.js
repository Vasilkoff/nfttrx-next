import { useState } from "react";


export default function useInput(initialState) {
    const [state, setState] = useState(initialState);

    const onChange = e => {
        if (e.target.type === "checkbox") {
            setState(e.target.checked)
        } else {

            setState(e.target.value)
        }
    }

    return {
        value: state,
        onChange,
    }
}