import { useEffect, useState } from "react";
import { db } from "../config/firebase";

type ReturnValueType = [isLoading: boolean, data: any[], isEmpty: boolean, error: string];

interface Options<T> {
  collection: string;
  where?: T;
  limit?: T;
}

// EXAMPLE 
    // const [isLoading, data, isEmpty, error] = useCollection({
    //   collection: "Users", 
    //   where: {
    //    email: "test@gmail.com",
    //    name: "Nfttrx",
    //    What ever you want 
    //   }
    // }, [deps]) 

export const useCollection = <T extends { [key: string]: string }, P>(
  options: Options<T>,
  deps: P[] = []
): ReturnValueType => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [isEmpty, setEmpty] = useState(true);
  const [error, setError] = useState("");

  const get = () => {
    const res = db.collection(options.collection);
    if (options?.where) {
      const arr = Object.keys(options.where);
      const result = arr.reduce(
        (akk: any, item) =>
          akk.where(
            item,
            "==",
            options?.where ? options?.where[item] : ""
          ),
        res
      );
      // TODO: write with limit method too 
      // code here...
      return result.get();
    }
    return res.get();
  };

  useEffect(() => {
    setLoading(true);

    get()
      .finally(() => setLoading(false))
      .then((res: any) => {
        
        setEmpty(res.empty);
        const parsedArray = res.docs.map((el: any) => el.data());
        setData(parsedArray);
      })
      .catch((error: any) => {
        setError(error.message)
      })
  }, [options.collection, options?.where, ...deps]);

  return [loading, data, isEmpty, error];
};
