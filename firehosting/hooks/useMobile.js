import { useLayoutEffect, useState } from "react";
import debounce from "lodash/debounce";

const useMobile = (forSteps) => {
  const [isMobile, setIsMobile] = useState(false);

  useLayoutEffect(() => {
    const updateSize = () => {
      setIsMobile(window.innerWidth < (forSteps || 768));
    };
    window.addEventListener("resize", debounce(updateSize, 250));
    updateSize();
    return () => window.removeEventListener("resize", updateSize);
  }, []);

  return isMobile;
};

export default useMobile;
