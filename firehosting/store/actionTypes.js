// Auth Types
export const AUTH_SUCCESS = "auth/AUTH_SUCCESS"
export const AUTH_FAIL = "auth/AUTH_FAIL"
export const AUTH_AVATAR = "auth/AUTH_AVATAR"
export const SET_NEW_USER = "auth/SET_NEW_USER"
export const AUTH_STEP_NEXT = "auth/AUTH_STEP_NEXT"

// Certificate Types

// Wallet Types
export const SET_WALLET = "wallet/SET_WALLET";
export const SET_TOKENS = "wallet/SET_TOKENS";
export const SET_NFTS = "wallet/SET_NFTS";
export const SET_NETWORK = "wallet/SET_NETWORK";
export const SET_SELECTED_WALLET = "wallet/SET_SELECTED_WALLET";

// Items Types
export const SET_USER_ITEMS = "items/SET_USER_ITEMS";
export const SET_ALL_ITEMS = "items/SET_ALL_ITEMS";
