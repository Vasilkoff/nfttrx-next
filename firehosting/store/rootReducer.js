import { combineReducers } from "redux";
import authReducer from "./reducers/authReducer.js";
import itemsReducer from "./reducers/itemsReducer.js";
import walletReducer from "./reducers/walletReducer.js";

const appReducer = combineReducers({
  auth: authReducer,
  wallet: walletReducer,
  items: itemsReducer,
});


const rootReducer = (state, action) => {
  if (action.type === 'USER_LOGOUT') {
    return appReducer(undefined, action)
  }

  return appReducer(state, action)
}

export default rootReducer;
