import {
  AUTH_FAIL,
  AUTH_SUCCESS,
  SET_NEW_USER,
  AUTH_STEP_NEXT,
} from "../actionTypes.js";
import stateUpdate from "../stateUpdate";

const initialState = {
  loading: true,
  error: "",
  data: null,
  register: {
    step: 1,
  },
};

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case SET_NEW_USER:
      return stateUpdate(state, {
        loading: false,
        error: "",
        data: {
          ...state.data,
          user: { ...state.data.user, ...action.payload },
        },
      });
    case AUTH_SUCCESS:
      return stateUpdate(state, {
        loading: false,
        error: "",
        data: action.payload,
        register: {
          step: action.payload?.emailVerified ? 3 : 2
        }
      });
    case AUTH_STEP_NEXT:
      return stateUpdate(state, { register: { step: action.payload } });
    case AUTH_FAIL:
      return stateUpdate(state, { loading: false, error: "", data: null });
    case AUTH_FAIL:
      return {
        ...state,
        data: {
          ...state.data,
          user: { ...state.data.user, avatar: action.payload },
        },
      };
    default:
      return state;
  }
}
