import stateUpdate from "../stateUpdate";

const initialState = {
  data: null,
};

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case "CERTIFICATE_REVIEW":
      return stateUpdate(state, { data: action.payload });
    default:
      return state;
  }
}