import { SET_ALL_ITEMS, SET_USER_ITEMS } from "../actionTypes.js";
import stateUpdate from "../stateUpdate";

const initialState = {
  userItemsLoading: true,
  allItemsLoading: true,
  userItems: [],
  allItems: [],
};

export default function itemsReducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_USER_ITEMS:
      return stateUpdate(state, { userItems: payload });
    case SET_ALL_ITEMS:
      return stateUpdate(state, { allItems: payload })
    default:
      return state;
  }
}