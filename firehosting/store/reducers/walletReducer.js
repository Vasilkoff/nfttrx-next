import { SET_NETWORK, SET_NFTS, SET_SELECTED_WALLET, SET_TOKENS, SET_WALLET } from "../actionTypes.js";
import stateUpdate from "../stateUpdate";

const initialState = {
  selectedWallet: null,
  selectedNetwork: "ropsten",
  data: [],
  tokensData: [],
  nftsData: [],
};

export default function walletReducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_WALLET:
      return stateUpdate(state, { data: payload });
    case SET_TOKENS:
      return stateUpdate(state, { tokensData: payload });
    case SET_NFTS:
      return stateUpdate(state, { nftsData: payload });
    case SET_NETWORK:
      return stateUpdate(state, { selectedNetwork: payload })
    case SET_SELECTED_WALLET:
      return stateUpdate(state, { selectedWallet: payload })
    default:
      return state;
  }
}