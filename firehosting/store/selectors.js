export const getAuth = (state) => state.auth.data;

export const getUserUid = (state) => state.auth.data?.uid;

export const getAuthStatus = (state) => state.auth.data?.emailVerified;

export const getUserMintedItems = (state) =>
  JSON.parse(state.auth.data?.user?.mintedNfts).map((item) => ({
    ...item,
    isMinted: true,
  })) || [];
