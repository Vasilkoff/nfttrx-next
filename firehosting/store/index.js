import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import reduxThunk from "redux-thunk";
import rootReducer from "./rootReducer";


const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(reduxThunk))
);


store.subscribe(() => {
  const {wallet} = store.getState();
  localStorage.setItem("selectedWallet", JSON.stringify(wallet.selectedWallet))
});

export default store;