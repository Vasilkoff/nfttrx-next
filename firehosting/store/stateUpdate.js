export default function stateUpdate(state, value) {
  return { ...state, ...value };
}
