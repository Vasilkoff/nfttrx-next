import {
  AUTH_AVATAR,
  AUTH_FAIL,
  AUTH_STEP_NEXT,
  AUTH_SUCCESS,
  SET_NEW_USER,
} from "../actionTypes.js";

export const setNewUser = (data) => ({
  type: SET_NEW_USER,
  payload: data,
});

export const setAuthSuccess = (data) => ({
  type: AUTH_SUCCESS,
  payload: data,
});

export const setAuthFail = () => ({
  type: AUTH_FAIL,
});

export const setUserAvatar = (payload) => ({
  type: AUTH_AVATAR,
  payload,
});

export const setRegisterStep = (payload) => ({
  type: AUTH_STEP_NEXT,
  payload,
});

export const setLogout = () => ({
  type: "USER_LOGOUT",
});
