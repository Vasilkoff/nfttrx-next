import firebase, { db } from "../../config/firebase.js";
import { setWallet, setSelectedWallet } from "./walletAction.js";
import { setAuthFail, setAuthSuccess } from "./authAction.js";
import { setUserItems, setAllItems } from "./itemsAction.js";

const getUserData = async (uid) => {
  const res = await db.collection("Users").doc(uid);
  const snapshot = await res.get();
  return snapshot.data();
}

const getUserItems = async (uid, status) => {
  const res = status 
    ? db.collection("UserItems").where("ownerUid", "==", uid).where("state", "==", status) 
    : db.collection("UserItems").where("ownerUid", "==", uid)
  const snapshot = await res.get();
  let arr = [];
  snapshot.forEach((doc) => {
    arr.push({ ...doc.data(), id: doc.id });
  });
  return arr
}

const getAllItems = async (uid, status) => {
  const res = db.collection("UserItems").where("state", "==", "listing") 
  const snapshot = await res.get();
  let arr = [];
  snapshot.forEach((doc) => {
    arr.push({ ...doc.data(), id: doc.id });
  });
  return arr
}


export const getUserItemsInitialize = (uid, status) => {
  return async (dispatch) => {
    const userItems = await getUserItems(uid, status) || [];
    dispatch(setUserItems(userItems))
  }
}
export const getAllItemsInitialize = () => {
  return async (dispatch) => {
    const userItems = await getAllItems() || [];
    dispatch(setAllItems(userItems))
  }
}

export const initializeRedux = () => (dispatch) => {
  const selectedWallet = JSON.parse(localStorage.getItem("selectedWallet"))
  dispatch(setSelectedWallet(selectedWallet));

  firebase.auth().onAuthStateChanged(async (user) => {
    if (user) {
      db.collection("Users").doc(user?.uid).collection("wallet")
        .onSnapshot((snapshot) => {
          const arr = [];
          snapshot.forEach((doc) => {
            arr.push({ ...doc.data(), id: doc.id });
          });
          if (arr.length) {
            dispatch(setWallet(arr))
            if (!selectedWallet) {
              dispatch(setSelectedWallet(arr[0]))
            }
          }
        })
      const res = await getUserData(user?.uid);
      dispatch(setAuthSuccess({ ...user?.toJSON(), user: res }));
      if (!res) {
        db.collection("Users").doc(user.uid).set({ email: user.email }, { merge: true })
      };
      dispatch(getUserItemsInitialize(user?.uid))
      // TODO: Нужно переделать получение сминченых nft так как кеширование работает не совсем коректно
      dispatch(getAllItemsInitialize()) 
    } else {
      dispatch(setAuthFail());
    }
  });
}
