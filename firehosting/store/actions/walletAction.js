import { SET_NETWORK, SET_NFTS, SET_SELECTED_WALLET, SET_TOKENS, SET_WALLET } from "../actionTypes.js";

export const setWallet = (payload) => ({
  type: SET_WALLET,
  payload
});

export const setTokens = (payload) => ({
  type: SET_TOKENS,
  payload
});

export const setNfts = (payload) => ({
  type: SET_NFTS,
  payload
});

export const setNetwork = payload => ({
  type: SET_NETWORK,
  payload
})

export const setSelectedWallet = payload => ({
  type: SET_SELECTED_WALLET,
  payload
})