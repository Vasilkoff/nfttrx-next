import { SET_USER_ITEMS, SET_ALL_ITEMS } from "../actionTypes.js";

export const setUserItems = (data) => ({
  type: SET_USER_ITEMS,
  payload: data,
});

export const setAllItems = (data) => ({
  type: SET_ALL_ITEMS,
  payload: data,
});
